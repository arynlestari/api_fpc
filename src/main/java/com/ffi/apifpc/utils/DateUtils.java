package com.ffi.apifpc.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtils {

    public static final String DATE_FORMAT_COMPLETE = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_dd_mm_yy = "dd-MM-yy";
    
    public static final String DATE_FORMAT_dd_mmm_Y = "dd-MMM-yyyy";
    public static final String DATE_FORMAT_D_M_Y = "dd-MM-yyyy";
    public static final String DATE_FORMAT_D_MMM_Y = "dd MMM yyyy";
    public static final String DATE_FORMAT_D_MMMM_Y = "dd MMMM yyyy";
    public static final String DATE_FORMAT_FORM = "MM/dd/yyyy";
    public static final String DATE_FORMAT_TIME = "HH:mm:ss";
    public static final String DATE_FORMAT_TIME_NEW = "HHmmss";

    public static Date newDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            Calendar currentDate = Calendar.getInstance();
            return sdf.parse(currentDate.getTime().toString());
        } catch (ParseException ex) {
            return new Date();
        }
    }

    public static Date newDate(int year, int month) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            Calendar currentDate = Calendar.getInstance();
            Date tempDate = currentDate.getTime();

            SimpleDateFormat sdf2 = new SimpleDateFormat("dd");
            String sDay = sdf2.format(tempDate);

            return sdf.parse(Integer.toString(year) + "-" + Integer.toString(month) + "-" + sDay);
        } catch (ParseException ex) {
            return new Date();
        }
    }

    public static Date newDateComplete() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_COMPLETE);
        try {
            String now = sdf.format(new Date());
            return sdf.parse(now);
        } catch (ParseException ex) {
            return new Date();
        }
    }

    public static Date newTimeComplete() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_TIME_NEW);
        try {
            String now = sdf.format(new Date());
            return sdf.parse(now);
        } catch (ParseException ex) {
            return new Date();
        }
    }

    public static Date newDate(int year, int month, int day) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            return sdf.parse(Integer.toString(year) + "-" + Integer.toString(month) + "-" + Integer.toString(day));
        } catch (ParseException ex) {
            return getLastDateOfMonth(year, month);
        }
    }

    public static Date newDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            return sdf.parse(date);
        } catch (ParseException ex) {
            return new Date();
        }
    }
    
    public static Date newDate(String date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            return sdf.parse(date);
        } catch (ParseException ex) {
            return new Date();
        }
    }

    public static Date newDateComplete(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_COMPLETE);
        try {
            return sdf.parse(date);
        } catch (ParseException ex) {
            return new Date();
        }
    }

    public static Date getFirstDateOfMonth(int year, int month) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            return sdf.parse(Integer.toString(year) + "-" + Integer.toString(month) + "-1");
        } catch (ParseException ex) {
            return new Date();
        }
    }

    public static String getFirstDateOfMonth_Form(int year, int month) {
        String tempMonth = Integer.toString(month);

        if (month < 10) {
            tempMonth = "0" + tempMonth;
        }

        return tempMonth + "/01/" + Integer.toString(year);
    }

    public static Date getLastDateOfMonth(int year, int month) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            Calendar currentDate = Calendar.getInstance();

            currentDate.set(year, month - 1, 1);

            int maxDay = currentDate.getActualMaximum(Calendar.DAY_OF_MONTH);

            return sdf.parse(Integer.toString(year) + "-" + Integer.toString(month) + "-" + Integer.toString(maxDay));
        } catch (ParseException ex) {
            return new Date();
        }
    }

    public static String getLastDateOfMonth_Form(int year, int month) {
        String tempMonth = Integer.toString(month);
        String tempDay = "";

        Calendar currentDate = Calendar.getInstance();

        currentDate.set(year, month - 1, 1);

        int maxDay = currentDate.getActualMaximum(Calendar.DAY_OF_MONTH);
        tempDay = Integer.toString(maxDay);

        if (maxDay < 10) {
            tempDay = "0" + tempDay;
        }

        if (month < 10) {
            tempMonth = "0" + tempMonth;
        }

        return tempMonth + "/" + tempDay + "/" + Integer.toString(year);
    }

    public static Date getFirstDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            Calendar currentDate = Calendar.getInstance();
            Date tempDate = currentDate.getTime();

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy");
            String sYear = sdf2.format(tempDate);

            sdf2 = new SimpleDateFormat("MM");
            String sMonth = sdf2.format(tempDate);

            return sdf.parse(sYear + "-" + sMonth + "-1");
        } catch (ParseException ex) {
            return new Date();
        }
    }

    public static Date getLastDate() {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        try {
            Calendar currentDate = Calendar.getInstance();
            Date tempDate = currentDate.getTime();

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy");
            String sYear = sdf2.format(tempDate);

            sdf2 = new SimpleDateFormat("MM");
            String sMonth = sdf2.format(tempDate);

            int maxDay = currentDate.getActualMaximum(Calendar.DAY_OF_MONTH);

            return sdf.parse(sYear + "-" + sMonth + "-" + Integer.toString(maxDay));
        } catch (ParseException ex) {
            return new Date();
        }
    }

    public static int getMaxDayOfMonth() {
        Calendar currentDate = Calendar.getInstance();

        return currentDate.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static String toString(Date date, String formatDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatDate);
        return sdf.format(date);
    }
    
    public static String getDayThisWeek() {
        Calendar currentDate = Calendar.getInstance();
        Date tempDate = currentDate.getTime();
        SimpleDateFormat format2 = new SimpleDateFormat("d/M/yyyy");
        String input =  format2.format(tempDate);
        DateTimeFormatter formatter = DateTimeFormat.forPattern( "d/M/yyyy" );
        LocalDate  localDate = formatter.parseLocalDate( input );
        Locale locale = Locale.US;        
        DateTimeFormatter formatterOutput = DateTimeFormat.forPattern( "EEEE" ).withLocale( locale );
        String output = formatterOutput.print( localDate );
        return output;
    }
    
    public static String newTime(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_TIME_NEW);
        try {
            Date dt =  sdf.parse(time);
            sdf = new SimpleDateFormat(DATE_FORMAT_TIME);
            String newtime = sdf.format(dt);
            return newtime;
        } catch (ParseException ex) {
            return "";
        }
    }
}