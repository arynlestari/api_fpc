/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Henda
 */
public class RptParameter {
    
    private String realName = "";
    private String description = "";
    private String stringValue = "";
    private Date dateValue = null;
    private BigDecimal decimalValue = BigDecimal.ZERO;
    private Integer integerValue = 0;
    private Character characterValue = new Character('0');
    private Long longValue = 0l;

    private boolean renderTypeSubType = false;
    private Long assetTypeValue;
    private Long assetSubTypeValue;

    private List<String> listOfType = new ArrayList<String>();
    private List<String> listOfSubType = new ArrayList<String>();
   
    private List<String> listOf = new ArrayList<String>();

    private Class valueClass;

    /**
     * @return the realName
     */
    public String getRealName() {
        return realName;
    }

    /**
     * @param realName the realName to set
     */
    public void setRealName(String realName) {
        this.realName = realName;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the stringValue
     */
    public String getStringValue() {
        return stringValue;
    }

    /**
     * @param stringValue the stringValue to set
     */
    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    /**
     * @return the dateValue
     */
    public Date getDateValue() {
        return dateValue;
    }

    /**
     * @param dateValue the dateValue to set
     */
    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    /**
     * @return the decimalValue
     */
    public BigDecimal getDecimalValue() {
        return decimalValue;
    }

    /**
     * @param decimalValue the decimalValue to set
     */
    public void setDecimalValue(BigDecimal decimalValue) {
        this.decimalValue = decimalValue;
    }

    /**
     * @return the integerValue
     */
    public Integer getIntegerValue() {
        return integerValue;
    }

    /**
     * @param integerValue the integerValue to set
     */
    public void setIntegerValue(Integer integerValue) {
        this.integerValue = integerValue;
    }

    /**
     * @return the characterValue
     */
    public Character getCharacterValue() {
        return characterValue;
    }

    /**
     * @param characterValue the characterValue to set
     */
    public void setCharacterValue(Character characterValue) {
        this.characterValue = characterValue;
    }

    /**
     * @return the longValue
     */
    public Long getLongValue() {
        return longValue;
    }

    /**
     * @param longValue the longValue to set
     */
    public void setLongValue(Long longValue) {
        this.longValue = longValue;
    }

    /**
     * @return the renderTypeSubType
     */
    public boolean isRenderTypeSubType() {
        return renderTypeSubType;
    }

    /**
     * @param renderTypeSubType the renderTypeSubType to set
     */
    public void setRenderTypeSubType(boolean renderTypeSubType) {
        this.renderTypeSubType = renderTypeSubType;
    }

    /**
     * @return the assetTypeValue
     */
    public Long getAssetTypeValue() {
        return assetTypeValue;
    }

    /**
     * @param assetTypeValue the assetTypeValue to set
     */
    public void setAssetTypeValue(Long assetTypeValue) {
        this.assetTypeValue = assetTypeValue;
    }

    /**
     * @return the assetSubTypeValue
     */
    public Long getAssetSubTypeValue() {
        return assetSubTypeValue;
    }

    /**
     * @param assetSubTypeValue the assetSubTypeValue to set
     */
    public void setAssetSubTypeValue(Long assetSubTypeValue) {
        this.assetSubTypeValue = assetSubTypeValue;
    }

    /**
     * @return the listOfType
     */
    public List<String> getListOfType() {
        return listOfType;
    }

    /**
     * @param listOfType the listOfType to set
     */
    public void setListOfType(List<String> listOfType) {
        this.listOfType = listOfType;
    }

    /**
     * @return the listOfSubType
     */
    public List<String> getListOfSubType() {
        return listOfSubType;
    }

    /**
     * @param listOfSubType the listOfSubType to set
     */
    public void setListOfSubType(List<String> listOfSubType) {
        this.listOfSubType = listOfSubType;
    }

    /**
     * @return the listOf
     */
    public List<String> getListOf() {
        return listOf;
    }

    /**
     * @param listOf the listOf to set
     */
    public void setListOf(List<String> listOf) {
        this.listOf = listOf;
    }

    /**
     * @return the valueClass
     */
    public Class getValueClass() {
        return valueClass;
    }

    /**
     * @param valueClass the valueClass to set
     */
    public void setValueClass(Class valueClass) {
        this.valueClass = valueClass;
    }
    
    
}
