/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.utils;


import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.formula.FormulaParser;
import org.apache.poi.ss.formula.FormulaRenderer;
import org.apache.poi.ss.formula.FormulaType;
import org.apache.poi.ss.formula.ptg.AreaPtg;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.formula.ptg.RefPtgBase;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFEvaluationWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author Henda
 */
public abstract class ExcelRptBase {

    protected XSSFWorkbook workbook;
    protected XSSFSheet sheet;
    protected FileInputStream input_document;
    protected ExcelExtractor excelExtractor;

    protected List<RptParameter> parameters = new ArrayList<RptParameter>();

    public abstract List<RptParameter> getParameters();

    public abstract String getTemplateName();

    public abstract String getTemplatePath();

    public String getTemplatePrefixPath() {
        return getClass().getProtectionDomain().getCodeSource().getLocation().getPath() + "corereportpoi/";
    }

    public XSSFWorkbook getWorkbook() throws Exception {

        if (workbook == null) {
            throw new Exception("gagal");
        }
        return workbook;
    }

    public void setWorkbook(XSSFWorkbook workbook) {
        this.workbook = workbook;
    }

    public XSSFSheet getSheet() throws Exception {
        if (sheet == null) {
            throw new Exception("gagal");
        }
        return sheet;
    }

    public void setSheet(XSSFSheet sheet) {
        this.sheet = sheet;
    }

    public void setParameters(List<RptParameter> parameters) {
        this.parameters = parameters;
    }

    public abstract void generate() throws Exception;

    protected void addFormulaCell(XSSFRow row, int cell, String formula, XSSFCellStyle style) {
        XSSFCell cellPost = row.createCell(cell);
        cellPost.setCellFormula(formula);
        cellPost.setCellStyle(style);
    }

    protected void addDecimalCell(XSSFRow row, int cell, BigDecimal value, XSSFCellStyle style) {
        XSSFCell cellPost = row.createCell(cell);
        cellPost.setCellValue(value.doubleValue());
        cellPost.setCellStyle(style);
    }

    protected void addNumericCell(XSSFRow row, int cell, Number value, XSSFCellStyle style) {
        XSSFCell cellPost = row.createCell(cell);
        cellPost.setCellValue(value.doubleValue());
        cellPost.setCellStyle(style);
    }

    protected void addDateCell(XSSFRow row, int cell, Date value, XSSFCellStyle style) {
        XSSFCell cellPost = row.createCell(cell);
        cellPost.setCellValue(value);
        cellPost.setCellStyle(style);
    }

    protected void addStringCell(XSSFRow row, int cell, String value, XSSFCellStyle style) {
        XSSFCell cellPost = row.createCell(cell);
        cellPost.setCellType(XSSFCell.CELL_TYPE_STRING);
        cellPost.setCellValue(value);
        cellPost.setCellStyle(style);
    }

    protected RptParameter getParameterByName(String parameterName) {
        for (RptParameter rpi : parameters) {
            if (rpi.getRealName().equals(parameterName)) {
                return rpi;
            }
        }

        return null;
    }

    @SuppressWarnings("deprecation")
    protected void loadWorkbook() throws IOException {
        try {
            workbook = new XSSFWorkbook(new FileInputStream(String.format("%s%s%s.xlsx", "", this.getTemplatePath(), this.getTemplateName())));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //input_document = new FileInputStream(new File(String.format("%s%s%s.xls", "", this.getTemplatePath(), this.getTemplateName())));
    }

    protected void loadWorkbookPath() throws IOException {
        try {
            workbook = new XSSFWorkbook(new FileInputStream(String.format("%s%s%s.xlsx", "", this.getTemplatePath(), this.getTemplateName())));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void loadSheet(String sheetName) throws Exception {
        sheet = getWorkbook().getSheet(sheetName);
    }

    protected void loadSheet(int sheetIndex) throws Exception {
        sheet = getWorkbook().getSheetAt(sheetIndex);
    }

    protected void updateCell(Integer columnIndex, Integer rowIndex, String value) throws Exception {

        XSSFRow row = getSheet().getRow(rowIndex);
        if (row == null) {
            row = getSheet().createRow(rowIndex);
        }
        XSSFCell cell = row.getCell(columnIndex);
        if (cell == null) {
            cell = row.createCell(columnIndex);
        }
        if (value != null) {
            cell.setCellValue(value);
        }

    }

    protected void updateCell(Integer columnIndex, Integer rowIndex, Calendar value) throws Exception {

        XSSFRow row = getSheet().getRow(rowIndex);
        if (row == null) {
            row = getSheet().createRow(rowIndex);
        }
        XSSFCell cell = row.getCell(columnIndex);
        if (cell == null) {
            cell = row.createCell(columnIndex);
        }
        if (value != null) {
            cell.setCellValue(value);
        }

    }

    protected void updateCell(Integer columnIndex, Integer rowIndex, Date value) throws Exception {

        XSSFRow row = getSheet().getRow(rowIndex);
        if (row == null) {
            row = getSheet().createRow(rowIndex);
        }
        XSSFCell cell = row.getCell(columnIndex);
        if (cell == null) {
            cell = row.createCell(columnIndex);
        }
        if (value != null) {
            cell.setCellValue(value);
        }

    }

    protected void updateCell(Integer columnIndex, Integer rowIndex, double value) throws Exception {

        XSSFRow row = getSheet().getRow(rowIndex);
        if (row == null) {
            row = getSheet().createRow(rowIndex);
        }
        XSSFCell cell = row.getCell(columnIndex);
        if (cell == null) {
            cell = row.createCell(columnIndex);
        }
        cell.setCellValue(value);

    }

    protected void updateCell(Integer columnIndex, Integer rowIndex, boolean value) throws Exception {

        XSSFRow row = getSheet().getRow(rowIndex);
        if (row == null) {
            row = getSheet().createRow(rowIndex);
        }
        XSSFCell cell = row.getCell(columnIndex);
        if (cell == null) {
            cell = row.createCell(columnIndex);
        }
        cell.setCellValue(value);

    }

    protected void updateCell(Integer columnIndex, Integer rowIndex, BigDecimal value) throws Exception {

        XSSFRow row = getSheet().getRow(rowIndex);
        if (row == null) {
            row = getSheet().createRow(rowIndex);
        }
        XSSFCell cell = row.getCell(columnIndex);
        if (cell == null) {
            cell = row.createCell(columnIndex);
        }
        if (value != null) {
            BigDecimal test = value.setScale(0, RoundingMode.HALF_UP);
            if (test != BigDecimal.ZERO) {
                cell.setCellValue(value.toString());
            }
        }

    }

    protected void updateCellFormula(Integer columnIndex, Integer rowIndex, String formula) throws Exception {

        XSSFRow row = getSheet().getRow(rowIndex);
        if (row == null) {
            row = getSheet().createRow(rowIndex);
        }
        XSSFCell cell = row.getCell(columnIndex);
        if (cell == null) {
            cell = row.createCell(columnIndex);
        }

        cell.setCellType(XSSFCell.CELL_TYPE_FORMULA);
        cell.setCellFormula(formula);

    }

    protected String pushToClient(String destinationPath, String fileName) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyy_HHmm");
        String today = formatter.format(new Date()).toString();
        //String defaultFileName = String.format("%s-%s-%s.xls", this.getTemplateName(), "USER", today);
        //String defaultFileName = destinationPath.concat(System.getProperty("file.separator")).concat(String.format("%s.xls", this.getTemplateName()));
        String defaultFileName = String.format("%s%s%s-%s.xlsx", "", destinationPath, this.getTemplateName(), fileName);
        System.out.println(defaultFileName);
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        getWorkbook().write(result);
        result.flush();
        byte[] bte = result.toByteArray();

        InputStream is = new ByteArrayInputStream(bte, 0, bte.length);
        result.close();
        FileOutputStream fileOut = new FileOutputStream(defaultFileName);
        getWorkbook().write(fileOut);
        fileOut.close();
        return defaultFileName;
        //FileUtil.pushToClient(defaultFileName, result.toByteArray());
    }
    
    protected String getFileName(String destinationPath, String fileName) throws Exception {
        return String.format("%s%s%s-%s.csv", "", destinationPath, this.getTemplateName(), fileName);
    }
    
    protected void pushToClient(HttpServletResponse response, String destinationPath, String fileName) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyy_HHmm");
        String today = formatter.format(new Date()).toString();
        //String defaultFileName = String.format("%s-%s-%s.xls", this.getTemplateName(), "USER", today);
        //String defaultFileName = destinationPath.concat(System.getProperty("file.separator")).concat(String.format("%s.xls", this.getTemplateName()));
        String defaultFileName = String.format("%s%s%s-%s.xlsx", "", destinationPath, this.getTemplateName(), fileName);
        System.out.println(defaultFileName);
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        getWorkbook().write(result);
        result.flush();
        byte[] bte = result.toByteArray();

        InputStream is = new ByteArrayInputStream(bte, 0, bte.length);
        result.close();
//        FileOutputStream fileOut = new FileOutputStream(defaultFileName);
//        getWorkbook().write(fileOut);
//        fileOut.close();
        IOUtils.copy(is, response.getOutputStream());
        response.flushBuffer();
        //return defaultFileName;
        //FileUtil.pushToClient(defaultFileName, result.toByteArray());
    }

    protected ByteArrayOutputStream pushByteToClient(String destinationPath) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyy_HHmm");
        String today = formatter.format(new Date()).toString();
        //String defaultFileName = String.format("%s-%s-%s.xls", this.getTemplateName(), "USER", today);
        //String defaultFileName = destinationPath.concat(System.getProperty("file.separator")).concat(String.format("%s.xls", this.getTemplateName()));
        String defaultFileName = String.format("%s%s%s-%s.xlsx", "", destinationPath, this.getTemplateName(), "rpt");
        System.out.println(defaultFileName);
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        getWorkbook().write(result);
        result.flush();
        byte[] bte = result.toByteArray();
        result.close();
        FileOutputStream fileOut = new FileOutputStream(defaultFileName);
        getWorkbook().write(fileOut);
        fileOut.close();
        return result;
    }

    protected void pushToClientCustomPath(String destinationPath) throws Exception {
        //HttpServletRequest request = ServletActionContext.getRequest();
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyy_HHmm");
        String today = formatter.format(new Date()).toString();
        //String defaultFileName = String.format("%s-%s-%s.xls", this.getTemplateName(), "USER", today);
        String defaultFileName = destinationPath.concat(System.getProperty("file.separator")).concat(String.format("%s-%s.xlsx", this.getTemplateName(), "rpt"));
        //String defaultFileName = request.getRealPath(String.format("%s%s%s-%s.xls", "", destinationPath, this.getTemplateName(), "rpt"));
        System.out.println(defaultFileName);
        ByteArrayOutputStream result = new ByteArrayOutputStream();

        getWorkbook().write(result);
        result.flush();
        byte[] bte = result.toByteArray();

        InputStream is = new ByteArrayInputStream(bte, 0, bte.length);
        result.close();
        FileOutputStream fileOut = new FileOutputStream(defaultFileName);
        getWorkbook().write(fileOut);
        fileOut.close();
        //FileUtil.pushToClient(defaultFileName, result.toByteArray());
    }

    protected void pushToPdfFile(String destinationPath) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("ddMMyy_HHmm");
        String today = formatter.format(new Date()).toString();
        String defaultFileName = String.format("%s-%s.pdf", this.getTemplateName(), "rpt");
        String excelFileName = String.format("%s%s%s.xlsx", "", this.getTemplatePath(), this.getTemplateName());
        System.out.println(defaultFileName);
        input_document = new FileInputStream(excelFileName);
        Iterator<Row> rowIterator = getSheet().iterator();
        //We will create output PDF document objects at this point

        Document iText_xls_2_pdf = new Document();
        PdfWriter.getInstance(iText_xls_2_pdf, new FileOutputStream(defaultFileName));
        iText_xls_2_pdf.open();

//        excelExtractor = new ExcelExtractor(getWorkbook());
        String filedata = excelExtractor.getText();
        iText_xls_2_pdf.add((Element) new Paragraph(filedata));
        iText_xls_2_pdf.close();
        //we created our pdf file..
        input_document.close(); //close xls

    }

    protected void mergeCell(int firstRow, int lastRow, int firstCol, int lastCol) {
        CellRangeAddress newCellRangeAddress = new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
        sheet.addMergedRegion(newCellRangeAddress);
    }

    protected void copyRow(int sourceRowNum, int destinationRowNum) {
        // Get the source / new row
        XSSFRow newRow = sheet.getRow(destinationRowNum);
        XSSFRow sourceRow = sheet.getRow(sourceRowNum);

        // If the row exist in destination, push down all rows by 1 else create a new row
        if (newRow != null) {
            sheet.shiftRows(destinationRowNum, sheet.getLastRowNum(), 1, true, true);
        } else {
            newRow = sheet.createRow(destinationRowNum);
        }
        if (sourceRow == null) {
            sourceRow = sheet.createRow(sourceRowNum);
        }

        sourceRow.getHeight();
        newRow.setHeight(sourceRow.getHeight());

        // Loop through source columns to add to new row
        for (int i = 0; i < sourceRow.getLastCellNum(); i++) {
            // Grab a copy of the old/new cell
            XSSFCell oldCell = sourceRow.getCell(i);
            XSSFCell newCell = newRow.createCell(i);

            // If the old cell is null jump to next cell
            if (oldCell == null) {
                newCell = null;
                continue;
            }

            // Copy style from old cell and apply to new cell
            XSSFCellStyle newCellStyle = workbook.createCellStyle();
            newCellStyle.cloneStyleFrom(oldCell.getCellStyle());

            newCell.setCellStyle(newCellStyle);

        }

        // If there are are any merged regions in the source row, copy to new row
        for (int i = 0; i < sheet.getNumMergedRegions(); i++) {
            CellRangeAddress cellRangeAddress = (CellRangeAddress) sheet.getMergedRegion(i);
            if (cellRangeAddress.getFirstRow() == sourceRow.getRowNum()) {
                CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.getRowNum(),
                        (newRow.getRowNum()
                        + (cellRangeAddress.getFirstRow()
                        - cellRangeAddress.getLastRow())),
                        cellRangeAddress.getFirstColumn(),
                        cellRangeAddress.getLastColumn());
                sheet.addMergedRegion(newCellRangeAddress);
            }
        }
    }

    protected void addBorderRightStyle(int oldRow, int newRow, int oldColumn, int newColumn) {
        XSSFRow sourceRow = sheet.getRow(oldRow);
        XSSFRow destinationRow = sheet.getRow(newRow);

        if (sourceRow == null) {
            sourceRow = sheet.createRow(oldRow);
        }
        if (destinationRow == null) {
            destinationRow = sheet.createRow(newRow);
        }

        XSSFCell oldCell = sourceRow.getCell(oldColumn);
        XSSFCell newCell = destinationRow.createCell(newColumn);

        // If the old cell is null jump to next cell
        if (oldCell == null) {
            newCell = null;
            return;
        }
//        copyCell(oldCell, newCell);

        // Copy style from old cell and apply to new cell
        XSSFCellStyle newCellStyle = workbook.createCellStyle();
        newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
        newCellStyle.setBorderRight(CellStyle.BORDER_THIN);
        newCell.setCellStyle(newCellStyle);

        // If there is a cell comment, copy
        if (newCell.getCellComment() != null) {
            newCell.setCellComment(oldCell.getCellComment());
        }

        // If there is a cell hyperlink, copy
        if (oldCell.getHyperlink() != null) {
            newCell.setHyperlink(oldCell.getHyperlink());
        }

        // Set the cell data type
        newCell.setCellType(oldCell.getCellType());
    }

    protected void copyColumnStyle(int oldRow, int newRow, int oldColumn, int newColumn) {
        XSSFRow sourceRow = sheet.getRow(oldRow);
        XSSFRow destinationRow = sheet.getRow(newRow);

        if (sourceRow == null) {
            sourceRow = sheet.createRow(oldRow);
        }
        if (destinationRow == null) {
            destinationRow = sheet.createRow(newRow);
        }

        XSSFCell oldCell = sourceRow.getCell(oldColumn);
        XSSFCell newCell = destinationRow.getCell(newColumn);
        if (newCell == null) {
            newCell = destinationRow.createCell(newColumn);
        }

        // If the old cell is null jump to next cell
        if (oldCell == null) {
            newCell = null;
            return;
        }
//        copyCell(oldCell, newCell);

        // Copy style from old cell and apply to new cell
        XSSFCellStyle newCellStyle = workbook.createCellStyle();
        newCellStyle.cloneStyleFrom(oldCell.getCellStyle());

        newCell.setCellStyle(newCellStyle);

//        // If there is a cell comment, copy
//        if (newCell.getCellComment() != null) {
//            newCell.setCellComment(oldCell.getCellComment());
//        }
//
//        // If there is a cell hyperlink, copy
//        if (oldCell.getHyperlink() != null) {
//            newCell.setHyperlink(oldCell.getHyperlink());
//        }
//
//        // Set the cell data type
//        newCell.setCellType(oldCell.getCellType());
    }

    public void copyRowStyle(int sourceRowNum, int destinationRowNum, int rowSize) {
        // Get the source / new row
        XSSFRow newRow = sheet.getRow(destinationRowNum);
        XSSFRow sourceRow = sheet.getRow(sourceRowNum);
        if (newRow == null) {
            newRow = sheet.createRow(destinationRowNum);
        }
        sourceRow.getHeight();
        newRow.setHeight(sourceRow.getHeight());
        XSSFCellStyle newCellStyle = workbook.createCellStyle();
        // Loop through source columns to add to new row
        for (int i = 0; i < rowSize; i++) {
            // Grab a copy of the old/new cell
            XSSFCell oldCell = sourceRow.getCell(i);
            XSSFCell newCell = newRow.createCell(i);
//            copyFormula(sheet, oldCell, newCell);
            // If the old cell is null jump to next cell
            if (oldCell != null) {
                newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
            }

            // Copy style from old cell and apply to new cell            
            newCell.setCellStyle(newCellStyle);
        }
    }

    protected void removeRow(XSSFSheet sheet, int rowIndex) {
        int lastRowNum = sheet.getLastRowNum();
        if (rowIndex >= 0 && rowIndex < lastRowNum) {
            sheet.shiftRows(rowIndex + 1, lastRowNum, -1);
        }
        if (rowIndex == lastRowNum) {
            XSSFRow removingRow = sheet.getRow(rowIndex);
            if (removingRow != null) {
                sheet.removeRow(removingRow);
            }
        }
    }

    protected void copyFormula(Sheet sheet, Cell org, Cell dest) {
        if (org == null || dest == null || sheet == null
                || org.getCellType() != Cell.CELL_TYPE_FORMULA) {
            return;
        }
        if (org.isPartOfArrayFormulaGroup()) {
            return;
        }
        String formula = org.getCellFormula();
        int shiftRows = dest.getRowIndex() - org.getRowIndex();
        int shiftCols = dest.getColumnIndex() - org.getColumnIndex();
        XSSFEvaluationWorkbook workbookWrapper
                = XSSFEvaluationWorkbook.create((XSSFWorkbook) sheet.getWorkbook());
        Ptg[] ptgs = FormulaParser.parse(formula, workbookWrapper, FormulaType.CELL, sheet.getWorkbook().getSheetIndex(sheet));
        for (Ptg ptg : ptgs) {
            if (ptg instanceof RefPtgBase) // base class for cell references
            {
                RefPtgBase ref = (RefPtgBase) ptg;
                if (ref.isColRelative()) {
                    ref.setColumn(ref.getColumn() + shiftCols);
                }
                if (ref.isRowRelative()) {
                    ref.setRow(ref.getRow() + shiftRows);
                }
            } else if (ptg instanceof AreaPtg) // base class for range references
            {
                AreaPtg ref = (AreaPtg) ptg;
                if (ref.isFirstColRelative()) {
                    ref.setFirstColumn(ref.getFirstColumn() + shiftCols);
                }
                if (ref.isLastColRelative()) {
                    ref.setLastColumn(ref.getLastColumn() + shiftCols);
                }
                if (ref.isFirstRowRelative()) {
                    ref.setFirstRow(ref.getFirstRow() + shiftRows);
                }
                if (ref.isLastRowRelative()) {
                    ref.setLastRow(ref.getLastRow() + shiftRows);
                }
            }
        }
        formula = FormulaRenderer.toFormulaString(workbookWrapper, ptgs);
        dest.setCellFormula(formula);
    }
}
