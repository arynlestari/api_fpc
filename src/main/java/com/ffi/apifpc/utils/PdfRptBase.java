/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.utils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author user
 */
public abstract class PdfRptBase {
    
    protected String pushToPdfFile(String fileName, String fileJasper, HashMap<String, Object> params) throws Exception {
        fileJasper = fileJasper.concat(".jasper");

        // 2.  Retrieve template
        InputStream reportStream = new FileInputStream("c:\\KFCO\\EMAIL\\"+fileJasper);//this.getClass().getResourceAsStream("/reports/" + fileJasper);
        // Make sure to pass the JasperReport, report parameters, and data source
        JasperPrint jp = JasperFillManager.fillReport(reportStream, params);
        fileName = fileName.concat(".pdf");
        try {
            JasperExportManager.exportReportToPdfFile(jp, "c:/KFCO/EMAIL/"+fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return fileName;
    }
}
