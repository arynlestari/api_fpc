/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HENDA
 */
public class ConnectionUtils {

    public static String CON_STRING_DB_LCL_DRIVER;// = "com.mysql.jdbc.Driver";
    public static String CON_STRING_DB_LCL_URL;// = "jdbc:mysql://localhost";
    public static String CON_STRING_DB_LCL_PORT;// = "";
    public static String CON_STRING_LCL_SID;// = "sst";
    public static String CON_STRING_DB_LCL_USER_NAME;// = "root";
    public static String CON_STRING_DB_LCL_PASSWORD;// = "mysql";
    
    public static String CON_STRING_SVR_DRIVER;// = "com.mysql.jdbc.Driver";
    public static final String CON_STRING_URL = "jdbc:mysql://172.16.2.200";
    public static String CON_STRING_SVR_URL;// = "jdbc:mysql://";
    public static String CON_STRING_SVR_PORT;// = "";
    public static String CON_STRING_SVR_SID;// = "pos";
    public static String CON_STRING_SVR_USER_NAME;// = "root";
    public static String CON_STRING_SVR_PASSWORD;// = "mysql";

    public static Connection getConnectionServer() throws Exception {
        //setConnection();
        Class.forName(CON_STRING_SVR_DRIVER);
        String url = "jdbc:oracle:thin:@(description=(address=(host=" + CON_STRING_SVR_URL + ")(protocol=tcp)(port=1521))(connect_data=(SERVICE_NAME = " + CON_STRING_SVR_SID + ")))";
        System.out.println("URL : "+url);
        return (Connection) DriverManager.getConnection(url, CON_STRING_SVR_USER_NAME, CON_STRING_SVR_PASSWORD);
    }
    
    public static Connection getConnectionSource(String uri) throws Exception {
        //setConnection();
        Class.forName(CON_STRING_SVR_DRIVER);
        String url = "jdbc:oracle:thin:@(description=(address=(host=" + uri + ")(protocol=tcp)(port=1521))(connect_data=(SERVICE_NAME = XE)))";
        return (Connection) DriverManager.getConnection(url, "kfc", "kfc");
    }

    public static Connection getConnectionLocal() throws Exception {
        //setConnection();
        Class.forName(CON_STRING_DB_LCL_DRIVER);
        String url = "jdbc:oracle:thin:@(description=(address=(host=" + CON_STRING_DB_LCL_URL + ")(protocol=tcp)(port=1521))(connect_data=(SERVICE_NAME = " + CON_STRING_LCL_SID + ")))";
        return (Connection) DriverManager.getConnection(url, CON_STRING_DB_LCL_USER_NAME, CON_STRING_DB_LCL_PASSWORD);
    }
    
    public static void setConnection(){
        Properties prop = new Properties();
        String dir = System.getProperty("user.dir");
        try {
            InputStream in = new FileInputStream("config.properties");
            prop.load(in);
            in.close();
            CON_STRING_DB_LCL_DRIVER = prop.getProperty("local_db_driver");
            CON_STRING_DB_LCL_URL = prop.getProperty("local_db_url");
            CON_STRING_DB_LCL_PORT = prop.getProperty("local_db_port");
            CON_STRING_LCL_SID = prop.getProperty("local_db_sid");
            CON_STRING_DB_LCL_USER_NAME = prop.getProperty("local_db_user");
            CON_STRING_DB_LCL_PASSWORD = prop.getProperty("local_db_password");
            /*Koneksi Server*/
            CON_STRING_SVR_DRIVER = prop.getProperty("svr_db_driver");
            CON_STRING_SVR_URL = prop.getProperty("svr_db_url");
            CON_STRING_SVR_PORT = prop.getProperty("svr_db_port");
            CON_STRING_SVR_SID = prop.getProperty("svr_db_sid");
            CON_STRING_SVR_USER_NAME = prop.getProperty("svr_db_user");
            CON_STRING_SVR_PASSWORD = prop.getProperty("svr_db_password");

        } catch (FileNotFoundException ex) {
            Logger.getLogger(ConnectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ConnectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
