/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.dao;

import com.ffi.apifpc.model.Budget;
import java.util.List;
import java.util.Map;

/**
 * 9 Jan 19
 * @author Aryn
 */
public interface ViewDAO {
    List<Map<String, Object>> listAllVehicle();
    List<Map<String, Object>> listVehicleByType(String vehicleType,String deptCode);
    List<Map<String, Object>> listAllBudget(String tahun,String dept);
    List<Map<String, Object>> listDeptByYear(String tahun);
    List<Map<String, Object>> listAllDept();
    List<Map<String, Object>> listRekap(String tahun);
    List<Map<String, Object>> listRekapDetail(String tahun,String platNo);
    List<Map<String, Object>> listDetail(String platNo);
    List<Map<String, Object>> listRekapDetailByDept(String tahun,String dept);
    List<Map<String, Object>> listRekapByDept(String tahun,String dept);
    List<Map<String, Object>> listAllVendor();
    List<Map<String, Object>> reportByDept(String currentMonth);
    List<Map<String, Object>> reportByRsc(String currentMonth);
    List<Map<String, Object>> reportBudgetOver20(String currentMonth);
    List<Map<String, Object>> reportByRscYear(String currentMonth);
    List<Map<String, Object>> listOpenWo(String param);
    List<Map<String, Object>> listOverBudget(String param);
    public String getSaldoTotal(Budget budget);
    List<Map<String, Object>> listSaldoBudget(String dept,String year);
    List<Map<String, Object>> listVehicleByDept(String param);
    List<Map<String, Object>> tesReport(String platNo);
    public String getCountWo(String wo);
    List<Map<String, Object>> listDetailServiceByDept(String dept);
}
