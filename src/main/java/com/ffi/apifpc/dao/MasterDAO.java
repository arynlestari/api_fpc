/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.dao;


import com.ffi.apifpc.model.Budget;
import com.ffi.apifpc.model.Department;
import com.ffi.apifpc.model.InputDetail;
import com.ffi.apifpc.model.InputHeader;
import com.ffi.apifpc.model.Realisasi;
import com.ffi.apifpc.model.WorkShop;
import java.util.List;

/**
 * 27 Dec 2018
 * @author aryn
 */
public interface MasterDAO {
    public void save(InputHeader inputHeader);
    public void delete (String platNo);
    public void update(InputHeader inputHeader);
    public void saveInput(InputHeader header,List<InputDetail> detail);
    public void saveDetail(InputDetail inputDetail);
    public void saveBudget(Budget budget);
    public void updateBudget(Budget budget);
    public void saveDepartment(Department dept);
    public void updateDepartment(Department dept);
    public void saveVendor(WorkShop vendor);
    public void updateVendor(WorkShop vendor);
    public void deleteVendor (String vendorCode);
    public void updateDetail(InputDetail inputDetail);
    public void deleteDetail(InputDetail idDetail);
    public void addBudget(Budget budget);
    public void editAdditionalBudget(Budget budget);
    public void realisasi(Realisasi realisasi);
}
