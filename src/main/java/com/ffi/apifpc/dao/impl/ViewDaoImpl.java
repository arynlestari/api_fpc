/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.dao.impl;

import com.ffi.apifpc.dao.ViewDAO;
import com.ffi.apifpc.model.Budget;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * 9 Jan 19
 *
 * @author Aryn
 */
@Repository
public class ViewDaoImpl implements ViewDAO {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public ViewDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Map<String, Object>> listAllVehicle() {
        String qry = "SELECT IH.*,D.DEPARTMENT_NAME FROM VEHICLE IH JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE";

        Map prm = new HashMap();
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("platNo", rs.getString(1));
                rt.put("pemakai", rs.getString(2));
                rt.put("departmentCode", rs.getString(3));
                rt.put("vehicle", rs.getString(4));
                rt.put("description", rs.getString(5));
                rt.put("vehicleType", rs.getString(6));
                rt.put("idVehicle", rs.getString(7));
                rt.put("tahun", rs.getString(8));
                rt.put("warna", rs.getString(9));
                rt.put("cc", rs.getString(10));
                rt.put("note", rs.getString(11));
                rt.put("departmentName", rs.getString(16));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listVehicleByType(String vehicleType, String deptCode) {
        String qry = "SELECT IH.*,D.DEPARTMENT_NAME FROM VEHICLE IH JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE WHERE IH.VEHICLE_TYPE=:vehicleType AND IH.DEPARTMENT_CODE like :departmentCode";
        Map prm = new HashMap();
        prm.put("vehicleType", vehicleType);
        prm.put("departmentCode", "%" + deptCode + "%");
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("platNo", rs.getString(1));
                rt.put("pemakai", rs.getString(2));
                rt.put("departmentCode", rs.getString(3));
                rt.put("vehicle", rs.getString(4));
                rt.put("description", rs.getString(5));
                rt.put("vehicleType", rs.getString(6));
                rt.put("idVehicle", rs.getString(7));
                rt.put("tahun", rs.getString(8));
                rt.put("warna", rs.getString(9));
                rt.put("cc", rs.getString(10));
                rt.put("note", rs.getString(11));
                rt.put("departmentName", rs.getString(16));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listAllBudget(String tahun, String dept) {
        String qry = "select B.*,D.DEPARTMENT_NAME from BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE\n"
                + "WHERE B.BUDGET_YEAR LIKE :tahun AND B.DEPARTMENT_CODE like :dept ";
//        String qry="SELECT AB.*,((NVL(JAN,0))+(NVL(FEB,0))+(NVL(MAR,0))+(NVL(APR,0))+(NVL(MEI,0))+(NVL(JUN,0))+(NVL(JUL,0))+(NVL(AGS,0))+(NVL(SEP,0))+(NVL(OKT,0))+(NVL(NOV,0))+(NVL(DES,0)))AS TOTALSEMUA FROM ("
//                + "select * from (select b.department_code,b.BUDGET_YEAR, b.budget_month monthbudget, b.total,D.DEPARTMENT_NAME from budget b \n"
//                   + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=b.DEPARTMENT_CODE where b.BUDGET_YEAR like :tahun and b.DEPARTMENT_CODE like :dept \n" +
//                   ")PIVOT (SUM(total) FOR (monthbudget) IN ('01' JAN,'02' FEB,'03' MAR,'04' APR,'05' MEI,'06' JUN,'07' JUL,'08' AGS,'09' SEP,'10' OKT,'11' NOV,'12' DES )) \n"
//                + "order by BUDGET_YEAR asc ) AB";
        Map prm = new HashMap();
        prm.put("tahun", "%" + tahun + "%");
        prm.put("dept", "%" + dept + "%");
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("departmentCode", rs.getString("department_code"));
                rt.put("total", rs.getString("total"));
                rt.put("departmentName", rs.getString("department_name"));
                rt.put("budgetYear", rs.getString("budget_year"));
                rt.put("id", rs.getString("id"));
                rt.put("inputDate", rs.getString("input_date"));
                rt.put("userCreate", rs.getString("user_create"));
                rt.put("userUpdate", rs.getString("user_update"));
                rt.put("dateUpdate", rs.getString("date_update"));
                rt.put("statusAdditional", rs.getString("status_additional"));
                rt.put("reasonAdditional", rs.getString("reason_additional"));
//                rt.put("jan", rs.getString(4)== null ? "0" : rs.getString(4));
//                rt.put("feb", rs.getString(5)== null ? "0" : rs.getString(5));
//                rt.put("mar", rs.getString(6)== null ? "0" : rs.getString(6));
//                rt.put("apr", rs.getString(7)== null ? "0" : rs.getString(7));
//                rt.put("mei", rs.getString(8)== null ? "0" : rs.getString(8));
//                rt.put("jun", rs.getString(9)== null ? "0" : rs.getString(9));
//                rt.put("jul", rs.getString(10)== null ? "0" : rs.getString(10));
//                rt.put("ags", rs.getString(11)== null ? "0" : rs.getString(11));
//                rt.put("sep", rs.getString(12)== null ? "0" : rs.getString(12));
//                rt.put("okt", rs.getString(13)== null ? "0" : rs.getString(13));
//                rt.put("nov", rs.getString(14)== null ? "0" : rs.getString(14));
//                rt.put("des", rs.getString(15)== null ? "0" : rs.getString(15));
//                rt.put("totalsemua", rs.getString(15)== null ? "0" : rs.getString(16));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listAllDept() {
        String qry = "SELECT * FROM DEPARTMENT ";
        Map prm = new HashMap();
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("departmentCode", rs.getString(1));
                rt.put("departmentName", rs.getString(2));
                rt.put("rsc", rs.getString(3));
                rt.put("coa", rs.getString(4));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listDeptByYear(String tahun) {
        String qry = "SELECT * FROM DEPARTMENT where DEPARTMENT_CODE not in "
                + "(select distinct BUDGET.DEPARTMENT_CODE from BUDGET where BUDGET.BUDGET_YEAR=:tahun) ";

        Map prm = new HashMap();
        prm.put("tahun", tahun);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("departmentCode", rs.getString(1));
                rt.put("departmentName", rs.getString(2));
                rt.put("rsc", rs.getString(3));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listRekap(String param) {
        String qry = "SELECT AB.*,((NVL(JAN,0))+(NVL(FEB,0))+(NVL(MAR,0))+(NVL(APR,0))+(NVL(MEI,0))+(NVL(JUN,0))+(NVL(JUL,0))+(NVL(AGS,0))+(NVL(SEP,0))+(NVL(OKT,0))+(NVL(NOV,0))+(NVL(DES,0)))AS TOTALSEMUA FROM ("
                + "select * from (select to_char(ID.wo_date, 'MM') as bulan,ID.plat_no,ID.total,IH.USED_BY,ID.DEPARTMENT_CODE,IH.VEHICLE,IH.DESCRIPTION,D.DEPARTMENT_NAME,IH.VEHICLE_TYPE \n"
                + " from WORK_ORDER ID LEFT JOIN VEHICLE IH on IH.PLAT_NO=ID.PLAT_NO JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=ID.DEPARTMENT_CODE where to_char(ID.wo_date,'yyyy')=:tahun )\n"
                + "pivot(sum(total) for bulan in ('01' JAN,'02' FEB,'03' MAR,'04' APR,'05' MEI,'06' JUN,'07' JUL,'08' AGS,'09' SEP,'10' OKT,'11' NOV,'12' DES ) \n"
                + ") order by plat_no asc ) AB";
        Map prm = new HashMap();
        prm.put("tahun", param);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("platNo", rs.getString(1));
                rt.put("usedBy", rs.getString(2));
                rt.put("department", rs.getString(3));
                rt.put("vehicle", rs.getString(4));
                rt.put("description", rs.getString(5));
                rt.put("departmentName", rs.getString(6));
                rt.put("vehicleType", rs.getString(7));
                rt.put("jan", rs.getString(8) == null ? "0" : rs.getString(8));
                rt.put("feb", rs.getString(9) == null ? "0" : rs.getString(9));
                rt.put("mar", rs.getString(10) == null ? "0" : rs.getString(10));
                rt.put("apr", rs.getString(11) == null ? "0" : rs.getString(11));
                rt.put("mei", rs.getString(12) == null ? "0" : rs.getString(12));
                rt.put("jun", rs.getString(13) == null ? "0" : rs.getString(13));
                rt.put("jul", rs.getString(14) == null ? "0" : rs.getString(14));
                rt.put("ags", rs.getString(15) == null ? "0" : rs.getString(15));
                rt.put("sep", rs.getString(16) == null ? "0" : rs.getString(16));
                rt.put("okt", rs.getString(17) == null ? "0" : rs.getString(17));
                rt.put("nov", rs.getString(18) == null ? "0" : rs.getString(18));
                rt.put("des", rs.getString(19) == null ? "0" : rs.getString(19));
                rt.put("totalsemua", rs.getString(20) == null ? "0" : rs.getString(20));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listRekapDetail(String tahun, String platNo) {
        String qry = "SELECT AB.*,((NVL(JAN,0))+(NVL(FEB,0))+(NVL(MAR,0))+(NVL(APR,0))+(NVL(MEI,0))+(NVL(JUN,0))+(NVL(JUL,0))+(NVL(AGS,0))+(NVL(SEP,0))+(NVL(OKT,0))+(NVL(NOV,0))+(NVL(DES,0)))AS TOTALSEMUA FROM ("
                + "select * from (select to_char(ID.wo_date, 'MM') as bulan,ID.PLAT_NO,ID.KILOMETER,NVL(ID.total,0) as total,ID.WORK_ORDER,ID.WORK_SHOP_CODE,ID.WO_DATE,ID.SPESIFICATION,D.DEPARTMENT_CODE,D.DEPARTMENT_NAME \n"
                + " from WORK_ORDER ID JOIN VEHICLE IH on IH.PLAT_NO=ID.PLAT_NO JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE where to_char(ID.wo_date,'yyyy')=:tahun and ID.PLAT_NO=:platNo ) \n"
                + "pivot ( sum(NVL(total,0)) for bulan in  ('01' JAN,'02' FEB,'03' MAR,'04' APR,'05' MEI,'06' JUN,'07' JUL,'08' AGS,'09' SEP,'10' OKT,'11' NOV,'12' DES ) \n"
                + ") order by wo_date asc ) AB";
        Map prm = new HashMap();
        prm.put("tahun", tahun);
        prm.put("platNo", platNo);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("platNo", rs.getString("PLAT_NO"));
                rt.put("kilometer", rs.getString("KILOMETER"));
                rt.put("workOrder", rs.getString("WORK_ORDER"));
                rt.put("workShopCode", rs.getString("WORK_SHOP_CODE"));
                rt.put("woDate", rs.getString("WO_DATE"));
                rt.put("spesification", rs.getString("SPESIFICATION"));
                rt.put("departmentCode", rs.getString("DEPARTMENT_CODE"));
                rt.put("departmentName", rs.getString("DEPARTMENT_NAME"));
                rt.put("jan", rs.getString("JAN") == null ? "0" : rs.getString("JAN"));
                rt.put("feb", rs.getString("FEB") == null ? "0" : rs.getString("FEB"));
                rt.put("mar", rs.getString("MAR") == null ? "0" : rs.getString("MAR"));
                rt.put("apr", rs.getString("APR") == null ? "0" : rs.getString("APR"));
                rt.put("mei", rs.getString("MEI") == null ? "0" : rs.getString("MEI"));
                rt.put("jun", rs.getString("JUN") == null ? "0" : rs.getString("JUN"));
                rt.put("jul", rs.getString("JUL") == null ? "0" : rs.getString("JUL"));
                rt.put("ags", rs.getString("AGS") == null ? "0" : rs.getString("AGS"));
                rt.put("sep", rs.getString("SEP") == null ? "0" : rs.getString("SEP"));
                rt.put("okt", rs.getString("OKT") == null ? "0" : rs.getString("OKT"));
                rt.put("nov", rs.getString("NOV") == null ? "0" : rs.getString("NOV"));
                rt.put("des", rs.getString("DES") == null ? "0" : rs.getString("DES"));
                rt.put("totalsemua", rs.getString("TOTALSEMUA") == null ? "0" : rs.getString("TOTALSEMUA"));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listDetail(String platNo) {
        String qry = "SELECT ID.*,W.WORK_SHOP_NAME,W.WORK_SHOP_ADDRESS,D.DEPARTMENT_NAME,NVL(R.TOTAL_REALISASI,0) AS TOTAL_REALISASI \n"
                + "FROM WORK_ORDER ID JOIN WORK_SHOP W ON W.WORK_SHOP_CODE=ID.WORK_SHOP_CODE JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=ID.DEPARTMENT_CODE\n"
                + "left JOIN REALISASI R ON R.PRINT_NUMBER=ID.PRINT_NUMBER WHERE ID.PLAT_NO=:platNo";
        Map prm = new HashMap();
        prm.put("platNo", platNo);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("kilometer", rs.getString("KILOMETER"));
                rt.put("platNo", rs.getString("PLAT_NO"));
                rt.put("workOrder", rs.getString("WORK_ORDER"));
                rt.put("workShopCode", rs.getString("WORK_SHOP_CODE"));
                rt.put("woDate", rs.getString("WO_DATE"));
                rt.put("spesification", rs.getString("SPESIFICATION"));
                rt.put("total", rs.getString("TOTAL"));
                rt.put("idVehicle", rs.getString("ID_VEHICLE"));
                rt.put("printNumber", rs.getString("PRINT_NUMBER"));
                rt.put("workShopName", rs.getString("WORK_SHOP_NAME"));
                rt.put("workShopAddress", rs.getString("WORK_SHOP_ADDRESS"));
                rt.put("statusRealisasi", rs.getString("STATUS_REALISASI"));
                rt.put("departmentCode", rs.getString("DEPARTMENT_CODE"));
                rt.put("departmentName", rs.getString("DEPARTMENT_NAME"));
                rt.put("totalRealisasi", rs.getString("TOTAL_REALISASI"));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listRekapDetailByDept(String tahun, String dept) {
        String qry = "SELECT AB.*,((NVL(JAN,0))+(NVL(FEB,0))+(NVL(MAR,0))+(NVL(APR,0))+(NVL(MEI,0))+(NVL(JUN,0))+(NVL(JUL,0))+(NVL(AGS,0))+(NVL(SEP,0))+(NVL(OKT,0))+(NVL(NOV,0))+(NVL(DES,0)))AS TOTALSEMUA FROM ("
                + "select * from (select to_char(ID.wo_date, 'MM') as bulan,ID.PLAT_NO,ID.KILOMETER,NVL(ID.total,0) as total,ID.WORK_ORDER,ID.WORK_SHOP_CODE,ID.WO_DATE,ID.SPESIFICATION,D.DEPARTMENT_CODE,D.DEPARTMENT_NAME \n"
                + " from WORK_ORDER ID JOIN VEHICLE IH on IH.PLAT_NO=ID.PLAT_NO JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE where to_char(ID.wo_date,'yyyy')=:tahun and IH.DEPARTMENT_CODE like :dept ) \n"
                + "pivot ( sum(NVL(total,0)) for bulan in  ('01' JAN,'02' FEB,'03' MAR,'04' APR,'05' MEI,'06' JUN,'07' JUL,'08' AGS,'09' SEP,'10' OKT,'11' NOV,'12' DES ) \n"
                + ") order by wo_date asc ) AB";
        Map prm = new HashMap();
        prm.put("tahun", tahun);
        prm.put("dept", "%" + dept + "%");
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("platNo", rs.getString("PLAT_NO"));
                rt.put("kilometer", rs.getString("KILOMETER"));
                rt.put("workOrder", rs.getString("WORK_ORDER"));
                rt.put("workShopCode", rs.getString("WORK_SHOP_CODE"));
                rt.put("woDate", rs.getString("WO_DATE"));
                rt.put("spesification", rs.getString("SPESIFICATION"));
                rt.put("departmentCode", rs.getString("DEPARTMENT_CODE"));
                rt.put("departmentName", rs.getString("DEPARTMENT_NAME"));
                rt.put("jan", rs.getString("JAN") == null ? "0" : rs.getString("JAN"));
                rt.put("feb", rs.getString("FEB") == null ? "0" : rs.getString("FEB"));
                rt.put("mar", rs.getString("MAR") == null ? "0" : rs.getString("MAR"));
                rt.put("apr", rs.getString("APR") == null ? "0" : rs.getString("APR"));
                rt.put("mei", rs.getString("MEI") == null ? "0" : rs.getString("MEI"));
                rt.put("jun", rs.getString("JUN") == null ? "0" : rs.getString("JUN"));
                rt.put("jul", rs.getString("JUL") == null ? "0" : rs.getString("JUL"));
                rt.put("ags", rs.getString("AGS") == null ? "0" : rs.getString("AGS"));
                rt.put("sep", rs.getString("SEP") == null ? "0" : rs.getString("SEP"));
                rt.put("okt", rs.getString("OKT") == null ? "0" : rs.getString("OKT"));
                rt.put("nov", rs.getString("NOV") == null ? "0" : rs.getString("NOV"));
                rt.put("des", rs.getString("DES") == null ? "0" : rs.getString("DES"));
                rt.put("totalsemua", rs.getString("TOTALSEMUA") == null ? "0" : rs.getString("TOTALSEMUA"));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listRekapByDept(String tahun, String dept) {
        String qry = "SELECT AB.*,((NVL(JAN,0))+(NVL(FEB,0))+(NVL(MAR,0))+(NVL(APR,0))+(NVL(MEI,0))+(NVL(JUN,0))+(NVL(JUL,0))+(NVL(AGS,0))+(NVL(SEP,0))+(NVL(OKT,0))+(NVL(NOV,0))+(NVL(DES,0)))AS TOTALSEMUA FROM ("
                + "select * from (select to_char(ID.wo_date, 'MM') as bulan,ID.total,D.DEPARTMENT_CODE,D.DEPARTMENT_NAME \n"
                + " from WORK_ORDER ID JOIN VEHICLE IH on IH.PLAT_NO=ID.PLAT_NO JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE where to_char(ID.wo_date,'yyyy')=:tahun and IH.DEPARTMENT_CODE like :dept ) \n"
                + "pivot ( sum(NVL(total,0)) for bulan in  ('01' JAN,'02' FEB,'03' MAR,'04' APR,'05' MEI,'06' JUN,'07' JUL,'08' AGS,'09' SEP,'10' OKT,'11' NOV,'12' DES ) \n"
                + ") order by DEPARTMENT_CODE asc) AB";
        Map prm = new HashMap();
        prm.put("tahun", tahun);
        prm.put("dept", "%" + dept + "%");
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("departmentCode", rs.getString(1));
                rt.put("departmentName", rs.getString(2));
                rt.put("jan", rs.getString(3) == null ? "0" : rs.getString(3));
                rt.put("feb", rs.getString(4) == null ? "0" : rs.getString(4));
                rt.put("mar", rs.getString(5) == null ? "0" : rs.getString(5));
                rt.put("apr", rs.getString(6) == null ? "0" : rs.getString(6));
                rt.put("mei", rs.getString(7) == null ? "0" : rs.getString(7));
                rt.put("jun", rs.getString(8) == null ? "0" : rs.getString(8));
                rt.put("jul", rs.getString(9) == null ? "0" : rs.getString(9));
                rt.put("ags", rs.getString(10) == null ? "0" : rs.getString(10));
                rt.put("sep", rs.getString(11) == null ? "0" : rs.getString(11));
                rt.put("okt", rs.getString(12) == null ? "0" : rs.getString(12));
                rt.put("nov", rs.getString(13) == null ? "0" : rs.getString(13));
                rt.put("des", rs.getString(14) == null ? "0" : rs.getString(14));
                rt.put("totalsemua", rs.getString(15) == null ? "0" : rs.getString(15));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listAllVendor() {
        String qry = "SELECT * FROM WORK_SHOP";

        Map prm = new HashMap();
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("workShopCode", rs.getString(1));
                rt.put("workShopName", rs.getString(2));
                rt.put("workShopAddress", rs.getString(3));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> reportByDept(String currentMonth) {
//        String qry ="SELECT DEPARTMENT_CODE,DEPARTMENT_NAME,RSC,SUM(JUM_PER_BLN) jml_servis_perbulan, \n" +
//                    "SUM(JUM_SD_BLN) jml_servis_sdbulan,SUM(realisasi_per_bln)realisasi_per_bln,SUM(realisasi_sd_bln) realisasi_sd_bln, \n" +
//                    "SUM(TOTAL_BUDGET_PERBULAN)TOTAL_BUDGET_PERBULAN,SUM(TOTAL_BUDGET_SDBULAN) TOTAL_BUDGET_SDBULAN," +
//                    "SUM(TOTAL_BUDGET_BULANDES)TOTAL_BUDGET_BULANDES,sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln) AS UNDEROVER  \n"+ 
//                    ",round(((case when sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln) < 0 then (sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln))*1 \n" +
//                    "else sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln) end) / (case when SUM(TOTAL_BUDGET_SDBULAN) >0 then SUM(TOTAL_BUDGET_SDBULAN) else 1 end))*100,2) AS PERSEN " +
//                    "FROM ( " +
//                    "select DEPARTMENT_CODE,DEPARTMENT_NAME,RSC,count(JUM_PER_BLN)JUM_PER_BLN,sum(JUM_SD_BLN)JUM_SD_BLN,sum(realisasi_per_bln)realisasi_per_bln,\n" +
//                    "sum(realisasi_sd_bln)realisasi_sd_bln,\n" +
//                    "sum(TOTAL_BUDGET_PERBULAN)TOTAL_BUDGET_PERBULAN,sum(TOTAL_BUDGET_SDBULAN) TOTAL_BUDGET_SDBULAN,SUM(TOTAL_BUDGET_BULANDES)TOTAL_BUDGET_BULANDES\n" +
//                    "from (\n" +
//                    "SELECT IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,COUNT(ID.PLAT_NO) AS JUM_PER_BLN, 0 JUM_SD_BLN,sum(ID.TOTAL) as realisasi_per_bln,\n" +
//                    "0 realisasi_sd_bln,0 TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,0 TOTAL_BUDGET_BULANDES\n" +
//                    "FROM WORK_ORDER ID JOIN VEHICLE IH ON IH.ID=ID.ID_VEHICLE\n" +
//                    "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE AND ID.PLAT_NO=IH.PLAT_NO\n" +
//                    "WHERE to_char(ID.wo_date,'MM yyyy')=:currentMonth GROUP BY IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,IH.PLAT_NO\n" +
//                    ") GROUP by DEPARTMENT_CODE,DEPARTMENT_NAME,rsc \n" +
//                    "UNION ALL " +
//                    "select DEPARTMENT_CODE,DEPARTMENT_NAME,RSC,sum(JUM_PER_BLN)JUM_PER_BLN,count(JUM_SD_BLN)JUM_SD_BLN,sum(realisasi_per_bln)realisasi_per_bln,\n" +
//                    "sum(realisasi_sd_bln)realisasi_sd_bln,sum(TOTAL_BUDGET_PERBULAN)TOTAL_BUDGET_PERBULAN,sum(TOTAL_BUDGET_SDBULAN)TOTAL_BUDGET_SDBULAN\n" +
//                    ",SUM(TOTAL_BUDGET_BULANDES)TOTAL_BUDGET_BULANDES from (\n" +
//                    "SELECT IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,0 JUM_PER_BLN,COUNT(IDD.PLAT_NO) AS JUM_SD_BLN,\n" +
//                    "0 realisasi_per_bln,sum(IDD.TOTAL) as realisasi_sd_bln,\n" +
//                    "0 TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,0 TOTAL_BUDGET_BULANDES\n" +
//                    "FROM WORK_ORDER IDD JOIN VEHICLE IH ON IH.ID=IDD.ID_VEHICLE\n" +
//                    "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE AND IDD.PLAT_NO=IH.PLAT_NO\n" +
//                    "WHERE to_char(IDD.wo_date,'MM yyyy') BETWEEN :fromMonth AND :toMonth GROUP BY IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,\n" +
//                    "IH.PLAT_NO)group by DEPARTMENT_CODE,DEPARTMENT_NAME,RSC\n" +
//                    "UNION ALL " +
//                    "SELECT B.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,0 JUM_PER_BLN,0 JUM_SD_BLN,0 realisasi_per_bln,0 realisasi_sd_bln,\n" +
//                    "SUM(B.TOTAL) TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,0 TOTAL_BUDGET_BULANDES \n" +
//                    "FROM BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE\n" +
//                    "WHERE B.budget_month||' '||B.budget_year=:currentMonth \n" +
//                    "GROUP BY B.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC\n" +
//                    "UNION ALL\n" +
//                    "SELECT B1.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,0 JUM_PER_BLN,0 JUM_SD_BLN,0 realisasi_per_bln,0 realisasi_sd_bln,\n" +
//                    "0 TOTAL_BUDGET_PERBULAN,SUM(B1.TOTAL) TOTAL_BUDGET_SDBULAN,0 TOTAL_BUDGET_BULANDES \n" +
//                    "FROM BUDGET B1 JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B1.DEPARTMENT_CODE \n" +
//                    "WHERE B1.budget_month||' '||B1.budget_year BETWEEN :fromMonth AND :toMonth \n" +
//                    "GROUP BY B1.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC \n"+
//                    "UNION ALL\n" +
//                    "SELECT B1.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,0 JUM_PER_BLN,0 JUM_SD_BLN,0 realisasi_per_bln,0 realisasi_sd_bln,\n" +
//                    "0 TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,SUM(B1.TOTAL) TOTAL_BUDGET_BULANDES \n" +
//                    "FROM BUDGET B1 JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B1.DEPARTMENT_CODE\n" +
//                    "WHERE B1.budget_month||' '||B1.budget_year BETWEEN :fromMonth AND :desMonth\n" +
//                    "GROUP BY B1.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC\n" +
//                    ") GROUP BY DEPARTMENT_CODE,DEPARTMENT_NAME,RSC";
        String qry = "SELECT DISTINCT DEPARTMENT_CODE,DEPARTMENT_NAME,RSC,SUM(JUM_SERVIS_CURRENTMONTH)JUM_SERVIS_CURRENTMONTH,\n"
                + "SUM(JUM_SERVIS_SD_CURRENTMONTH)JUM_SERVIS_SD_CURRENTMONTH,\n"
                + "SUM(BIAYA_SERVIS_CURRENTMONTH)BIAYA_SERVIS_CURRENTMONTH,SUM(BIAYA_SERVIS_SD_CURRENTMONTH)BIAYA_SERVIS_SD_CURRENTMONTH,\n"
                + "SUM(REALISASI_CURRENTMONTH)REALISASI_CURRENTMONTH,SUM(REALISASI_SD_CURRENTMONTH)REALISASI_SD_CURRENTMONTH,\n"
                + "SUM(TOTAL_BUDGET_TAHUN)TOTAL_BUDGET_TAHUN,SUM(OVER_BUDGET)OVER_BUDGET FROM(\n"
                + "-- jumlah kendaraan diservis bulan tertentu\n"
                + "SELECT ID.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,NVL(COUNT(ID.PLAT_NO),0) AS JUM_SERVIS_CURRENTMONTH,0 JUM_SERVIS_SD_CURRENTMONTH,\n"
                + "SUM(ID.TOTAL)BIAYA_SERVIS_CURRENTMONTH,0 BIAYA_SERVIS_SD_CURRENTMONTH,0 REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH,\n"
                + "0 TOTAL_BUDGET_TAHUN,0 OVER_BUDGET\n"
                + "FROM WORK_ORDER ID LEFT JOIN VEHICLE IH ON IH.ID=ID.ID_VEHICLE\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=ID.DEPARTMENT_CODE -- AND ID.PLAT_NO=IH.PLAT_NO AND ID.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n"
                + "WHERE to_char(ID.wo_date,'MM yyyy')=:currentMonth GROUP BY ID.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC\n"
                + "union all\n"
                + "-- jumlah kendaraan diservis dari januari s/d bulan tertentu\n"
                + "SELECT ID.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,0 JUM_SERVIS_CURRENTMONTH,NVL(COUNT(ID.PLAT_NO),0) JUM_SERVIS_SD_CURRENTMONTH,\n"
                + "0 BIAYA_SERVIS_CURRENTMONTH,SUM(ID.TOTAL)BIAYA_SERVIS_SD_CURRENTMONTH,0 REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH,\n"
                + "0 TOTAL_BUDGET_TAHUN,0 OVER_BUDGET\n"
                + "FROM WORK_ORDER ID LEFT JOIN VEHICLE IH ON IH.ID=ID.ID_VEHICLE\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=ID.DEPARTMENT_CODE -- AND ID.PLAT_NO=IH.PLAT_NO AND ID.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n"
                + "WHERE to_char(ID.wo_date,'MM yyyy') BETWEEN :fromMonth AND :toMonth GROUP BY ID.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC\n"
                + "UNION ALL\n"
                + "-- jumlah realisasi servis kendaraan bulan tertentu\n"
                + "SELECT WO.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,0 JUM_SERVIS_CURRENTMONTH,0 JUM_SERVIS_SD_CURRENTMONTH,0 BIAYA_SERVIS_CURRENTMONTH, \n"
                + "0 BIAYA_SERVIS_SD_CURRENTMONTH,SUM(TOTAL_REALISASI) AS REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH,0 TOTAL_BUDGET_TAHUN,0 OVER_BUDGET \n"
                + "FROM REALISASI R\n"
                + "JOIN WORK_ORDER WO ON WO.PRINT_NUMBER=R.PRINT_NUMBER\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=WO.DEPARTMENT_CODE WHERE to_char(R.TGL_REALISASI,'MM yyyy')=:currentMonth\n"
                + "GROUP BY WO.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC\n"
                + "UNION ALL\n"
                + "-- jumlah realisasi servis kendaraan s/d bulan tertentu\n"
                + "SELECT WO.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,0 JUM_SERVIS_CURRENTMONTH,0 JUM_SERVIS_SD_CURRENTMONTH,0 BIAYA_SERVIS_CURRENTMONTH, \n"
                + "0 BIAYA_SERVIS_SD_CURRENTMONTH,0 REALISASI_CURRENTMONTH,SUM(TOTAL_REALISASI) REALISASI_SD_CURRENTMONTH,0 TOTAL_BUDGET_TAHUN,0 OVER_BUDGET \n"
                + "FROM REALISASI R\n"
                + "JOIN WORK_ORDER WO ON WO.PRINT_NUMBER=R.PRINT_NUMBER\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=WO.DEPARTMENT_CODE WHERE to_char(R.TGL_REALISASI,'MM yyyy') BETWEEN :fromMonth AND :toMonth\n"
                + "GROUP BY WO.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC\n"
                + "UNION ALL\n"
                + "-- jumlah budget per tahun\n"
                + "SELECT B.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,0 JUM_SERVIS_CURRENTMONTH,0 JUM_SERVIS_SD_CURRENTMONTH,0 BIAYA_SERVIS_CURRENTMONTH,\n"
                + "0 BIAYA_SERVIS_SD_CURRENTMONTH,0 REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH,SUM(B.TOTAL)TOTAL_BUDGET_TAHUN,0 OVER_BUDGET\n"
                + "FROM BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE WHERE B.BUDGET_YEAR=:year AND B.STATUS_ADDITIONAL='N'\n"
                + "GROUP BY B.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC\n"
                + "UNION all\n"
                + "-- jumlah budget tambahan/over\n"
                + "SELECT B.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC,0 JUM_SERVIS_CURRENTMONTH,0 JUM_SERVIS_SD_CURRENTMONTH,0 BIAYA_SERVIS_CURRENTMONTH,\n"
                + "0 BIAYA_SERVIS_SD_CURRENTMONTH,0 REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH,0TOTAL_BUDGET_TAHUN,SUM(B.TOTAL)OVER_BUDGET\n"
                + "FROM BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE WHERE B.BUDGET_YEAR=:year AND B.STATUS_ADDITIONAL='Y'\n"
                + "GROUP BY B.DEPARTMENT_CODE,D.DEPARTMENT_NAME,D.RSC\n"
                + ")GROUP BY DEPARTMENT_CODE,DEPARTMENT_NAME,RSC";
        Map prm = new HashMap();
        prm.put("currentMonth", currentMonth);
        String a = currentMonth.substring(3, 7);
        String fromMonth = "01 " + a;
        String desMonth = "12 " + a;
        prm.put("fromMonth", fromMonth);
        prm.put("toMonth", currentMonth);
        prm.put("year", a);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("departmentCode", rs.getString("DEPARTMENT_CODE"));
                rt.put("departmentName", rs.getString("DEPARTMENT_NAME"));
                rt.put("rsc", rs.getString("RSC"));
                rt.put("jumServisCurrentMonth", rs.getString("jum_servis_currentmonth") == null ? "0" : rs.getString("jum_servis_currentmonth"));
                rt.put("jumServisSdCurrentMonth", rs.getString("jum_servis_sd_currentmonth") == null ? "0" : rs.getString("jum_servis_sd_currentmonth"));
                rt.put("biayaServisCurrentMonth", rs.getString("biaya_servis_currentMonth") == null ? "0" : rs.getString("biaya_servis_currentMonth"));
                rt.put("biayaServisSdCurrentMonth", rs.getString("biaya_servis_sd_currentMonth") == null ? "0" : rs.getString("biaya_servis_sd_currentMonth"));
                rt.put("realisasiCurrentMonth", rs.getString("realisasi_currentMonth") == null ? "0" : rs.getString("realisasi_currentMonth"));
                rt.put("realisasiSdCurrentMonth", rs.getString("realisasi_sd_currentMonth") == null ? "0" : rs.getString("realisasi_sd_currentMonth"));
                rt.put("totalBudget", rs.getString("total_budget_tahun") == null ? "0" : rs.getString("total_budget_tahun"));
                rt.put("overBudget", rs.getString("over_budget") == null ? "0" : rs.getString("over_budget"));

                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> reportByRsc(String currentMonth) {
//        String qry ="SELECT RSC,SUM(JUM_PER_BLN) diservis,\n" +
//                "SUM(JUM_SD_BLN) terdaftar,SUM(realisasi_per_bln)realisasi_per_bln,SUM(realisasi_sd_bln) realisasi_sd_bln,\n" +
//                "SUM(TOTAL_BUDGET_PERBULAN)TOTAL_BUDGET_PERBULAN,SUM(TOTAL_BUDGET_SDBULAN) TOTAL_BUDGET_SDBULAN,\n" +
//                "SUM(TOTAL_BUDGET_BULANDES)TOTAL_BUDGET_BULANDES,sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln) AS UNDEROVER\n" +
//                ",round(((case when sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln) < 0 then (sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln))*1 \n" +
//                "else sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln) end)\n" +
//                "/ (case when SUM(TOTAL_BUDGET_SDBULAN) >0 then SUM(TOTAL_BUDGET_SDBULAN) else 1 end))*100,2) AS PERSEN\n" +
//                "FROM (\n" +
//                "SELECT RSC,COUNT(JUM_PER_BLN)JUM_PER_BLN,SUM(JUM_SD_BLN)JUM_SD_BLN,SUM(realisasi_per_bln)realisasi_per_bln,\n" +
//                "SUM(realisasi_sd_bln)realisasi_sd_bln,SUM(TOTAL_BUDGET_PERBULAN)TOTAL_BUDGET_PERBULAN,SUM(TOTAL_BUDGET_SDBULAN)TOTAL_BUDGET_SDBULAN\n" +
//                ",SUM(TOTAL_BUDGET_BULANDES)TOTAL_BUDGET_BULANDES FROM (\n" +
//                "SELECT D.RSC,COUNT(IH.PLAT_NO) AS JUM_PER_BLN, 0 JUM_SD_BLN,sum(ID.TOTAL) as realisasi_per_bln,0 realisasi_sd_bln,\n" +
//                "0 TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,0 TOTAL_BUDGET_BULANDES\n" +
//                "FROM WORK_ORDER ID JOIN VEHICLE IH ON IH.ID=ID.ID_VEHICLE\n" +
//                "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE AND ID.PLAT_NO=IH.PLAT_NO\n" +
//                "WHERE to_char(ID.wo_date,'MM yyyy')=:currentMonth GROUP BY D.RSC,IH.PLAT_NO\n" +
//                ")GROUP BY RSC\n" +
//                "UNION ALL\n" +
//                "SELECT D.RSC,0 JUM_PER_BLN,0 AS JUM_SD_BLN,0 realisasi_per_bln,sum(IDD.TOTAL) as realisasi_sd_bln,\n" +
//                "0 TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,0 TOTAL_BUDGET_BULANDES\n" +
//                "FROM WORK_ORDER IDD JOIN VEHICLE IH ON IH.ID=IDD.ID_VEHICLE\n" +
//                "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n" +
//                "WHERE to_char(IDD.wo_date,'MM yyyy') BETWEEN :fromMonth AND :toMonth GROUP BY D.RSC\n" +
//                "UNION ALL\n" +
//                "SELECT D.RSC,0 JUM_PER_BLN,COUNT(IH.PLAT_NO) AS JUM_SD_BLN,0 realisasi_per_bln,0 as realisasi_sd_bln,\n" +
//                "0 TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,0 TOTAL_BUDGET_BULANDES\n" +
//                "FROM VEHICLE IH JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n" +
//                "GROUP BY D.RSC\n" +
//                "UNION ALL\n" +
//                "SELECT D.RSC,0 JUM_PER_BLN,0 JUM_SD_BLN,0 realisasi_per_bln,0 realisasi_sd_bln,\n" +
//                "SUM(B.TOTAL) TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,0 TOTAL_BUDGET_BULANDES\n" +
//                "FROM BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE\n" +
//                "WHERE B.budget_month||' '||B.budget_year=:currentMonth \n" +
//                "GROUP BY D.RSC\n" +
//                "UNION ALL\n" +
//                "SELECT D.RSC,0 JUM_PER_BLN,0 JUM_SD_BLN,0 realisasi_per_bln,0 realisasi_sd_bln,\n" +
//                "0 TOTAL_BUDGET_PERBULAN,SUM(B1.TOTAL) TOTAL_BUDGET_SDBULAN,0 TOTAL_BUDGET_BULANDES \n" +
//                "FROM BUDGET B1 JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B1.DEPARTMENT_CODE\n" +
//                "WHERE B1.budget_month||' '||B1.budget_year BETWEEN :fromMonth AND :toMonth \n" +
//                "GROUP BY D.RSC \n"+
//                "UNION ALL\n" +
//                "SELECT D.RSC,0 JUM_PER_BLN,0 JUM_SD_BLN,0 realisasi_per_bln,0 realisasi_sd_bln,\n" +
//                "0 TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,SUM(B1.TOTAL) TOTAL_BUDGET_BULANDES \n" +
//                "FROM BUDGET B1 JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B1.DEPARTMENT_CODE\n" +
//                "WHERE B1.budget_month||' '||B1.budget_year BETWEEN :fromMonth AND :desMonth\n" +
//                "GROUP BY D.RSC\n" +
//                ") GROUP BY RSC";
        String qry = "SELECT RSC,SUM(TERDAFTAR)TERDAFTAR,\n"
                + "SUM(DISERVIS)DISERVIS,SUM(BIAYA_SERVIS_SD_CURRENTMONTH)BIAYA_SERVIS,\n"
                + "SUM(REALISASI_CURRENTMONTH)REALISASI_CURRENTMONTH,SUM(REALISASI_SD_CURRENTMONTH)REALISASI_SD_CURRENTMONTH,\n"
                + "SUM(TOTAL_BUDGET_TAHUN)TOTAL_BUDGET_TAHUN,SUM(OVER_BUDGET)OVER_BUDGET FROM(\n"
                + "-- jumlah kendaraan terdaftar bulan tertentu\n"
                + "SELECT D.RSC,COUNT(IH.PLAT_NO) TERDAFTAR,0 DISERVIS,\n"
                + "0 BIAYA_SERVIS_SD_CURRENTMONTH,0 REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH,\n"
                + "0 TOTAL_BUDGET_TAHUN,0 OVER_BUDGET\n"
                + "FROM VEHICLE IH JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n"
                + "GROUP BY D.RSC\n"
                + "union all\n"
                + "-- jumlah kendaraan diservis dari januari s/d bulan tertentu\n"
                + "SELECT D.RSC,0 TERDAFTAR,NVL(COUNT(ID.PLAT_NO),0) DISERVIS,\n"
                + "SUM(ID.TOTAL)BIAYA_SERVIS_SD_CURRENTMONTH,0 REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH,\n"
                + "0 TOTAL_BUDGET_TAHUN,0 OVER_BUDGET\n"
                + "FROM WORK_ORDER ID JOIN VEHICLE IH ON IH.ID=ID.ID_VEHICLE\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=ID.DEPARTMENT_CODE AND ID.PLAT_NO=IH.PLAT_NO AND ID.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n"
                + "WHERE to_char(ID.wo_date,'MM yyyy') BETWEEN :fromMonth AND :toMonth GROUP BY D.RSC\n"
                + "UNION ALL\n"
                + "-- jumlah realisasi servis kendaraan bulan tertentu\n"
                + "SELECT D.RSC,0 TERDAFTAR,0 DISERVIS,0 BIAYA_SERVIS_CURRENTMONTH, \n"
                + "SUM(TOTAL_REALISASI) AS REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH,0 TOTAL_BUDGET_TAHUN,0 OVER_BUDGET \n"
                + "FROM REALISASI R\n"
                + "JOIN WORK_ORDER WO ON WO.PRINT_NUMBER=R.PRINT_NUMBER\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=WO.DEPARTMENT_CODE WHERE to_char(R.TGL_REALISASI,'MM yyyy')=:currentMonth\n"
                + "GROUP BY D.RSC\n"
                + "UNION ALL\n"
                + "-- jumlah realisasi servis kendaraan s/d bulan tertentu\n"
                + "SELECT D.RSC,0 TERDAFTAR,0 DISERVIS, \n"
                + "0 BIAYA_SERVIS_SD_CURRENTMONTH,0 REALISASI_CURRENTMONTH,SUM(TOTAL_REALISASI) REALISASI_SD_CURRENTMONTH,0 TOTAL_BUDGET_TAHUN,0 OVER_BUDGET \n"
                + "FROM REALISASI R\n"
                + "JOIN WORK_ORDER WO ON WO.PRINT_NUMBER=R.PRINT_NUMBER\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=WO.DEPARTMENT_CODE WHERE to_char(R.TGL_REALISASI,'MM yyyy') BETWEEN :fromMonth AND :toMonth\n"
                + "GROUP BY D.RSC\n"
                + "UNION ALL\n"
                + "-- jumlah budget per tahun\n"
                + "SELECT D.RSC,0 TERDAFTAR,0 DISERVIS,\n"
                + "0 BIAYA_SERVIS_SD_CURRENTMONTH,0 REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH,SUM(B.TOTAL)TOTAL_BUDGET_TAHUN,0 OVER_BUDGET\n"
                + "FROM BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE WHERE B.BUDGET_YEAR=:year AND B.STATUS_ADDITIONAL='N'\n"
                + "GROUP BY D.RSC\n"
                + "UNION all\n"
                + "-- jumlah budget tambahan/over\n"
                + "SELECT D.RSC,0 TERDAFTAR,0 DISERVIS,\n"
                + "0 BIAYA_SERVIS_SD_CURRENTMONTH,0 REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH,0TOTAL_BUDGET_TAHUN,SUM(B.TOTAL)OVER_BUDGET\n"
                + "FROM BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE WHERE B.BUDGET_YEAR=:year AND B.STATUS_ADDITIONAL='Y'\n"
                + "GROUP BY D.RSC\n"
                + ")GROUP BY RSC";
        Map prm = new HashMap();
        prm.put("currentMonth", currentMonth);
        String a = currentMonth.substring(3, 7);
        String fromMonth = "01 " + a;
        String desMonth = "12 " + a;
        prm.put("fromMonth", fromMonth);
        prm.put("toMonth", currentMonth);
        prm.put("year", a);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("rsc", rs.getString(1));
                rt.put("terdaftar", rs.getString(2) == null ? "0" : rs.getString(2));
                rt.put("diservis", rs.getString(3) == null ? "0" : rs.getString(3));
                rt.put("biayaServis", rs.getString(4) == null ? "0" : rs.getString(4));
                rt.put("realisasiCurrentMonth", rs.getString(5) == null ? "0" : rs.getString(5));
                rt.put("realisasiSdCurrentMonth", rs.getString(6) == null ? "0" : rs.getString(6));
                rt.put("totalBudgetTahun", rs.getString(7) == null ? "0" : rs.getString(7));
                rt.put("overBudget", rs.getString(8) == null ? "0" : rs.getString(8));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> reportBudgetOver20(String currentMonth) {
        /*        String qry ="SELECT * FROM (\n" +
                    "SELECT DEPARTMENT_CODE,DEPARTMENT_NAME,\n" +
                    "SUM(TOTAL_BUDGET_PERBULAN)TOTAL_BUDGET_PERBULAN,\n" +
                    "SUM(TOTAL_BUDGET_SDBULAN) TOTAL_BUDGET_SDBULAN,\n" +
                    "SUM(realisasi_per_bln)realisasi_per_bln,\n" +
                    "SUM(realisasi_sd_bln) realisasi_sd_bln,\n" +
                    "sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln) AS UNDEROVER\n" +
                    ",round(((case when sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln) < 0 then (sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln))*1 \n" +
                    "else sum(TOTAL_BUDGET_SDBULAN-realisasi_sd_bln) end)\n" +
                    "/ (case when SUM(TOTAL_BUDGET_SDBULAN) > 0 then SUM(TOTAL_BUDGET_SDBULAN) else 1 end))*100,2) PERSEN\n" +
                    "FROM (\n" +
                    "SELECT IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME,0 TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,\n" +
                    "sum(ID.TOTAL) as realisasi_per_bln,0 realisasi_sd_bln\n" +
                    "FROM WORK_ORDER ID JOIN VEHICLE IH ON IH.ID=ID.ID_VEHICLE\n" +
                    "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n" +
                    "WHERE to_char(ID.wo_date,'MM yyyy')=:currentMonth GROUP BY IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME\n" +
                    "UNION ALL\n" +
                    "SELECT IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME,0 TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,\n" +
                    "0 realisasi_per_bln,sum(IDD.TOTAL) as realisasi_sd_bln\n" +
                    "FROM WORK_ORDER IDD JOIN VEHICLE IH ON IH.ID=IDD.ID_VEHICLE\n" +
                    "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n" +
                    "WHERE to_char(IDD.wo_date,'MM yyyy') BETWEEN :fromMonth AND :toMonth  GROUP BY IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME\n" +
                    "UNION ALL\n" +
                    "SELECT B.DEPARTMENT_CODE,D.DEPARTMENT_NAME,SUM(B.TOTAL) TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,\n" +
                    "0 realisasi_per_bln,0 realisasi_sd_bln\n" +
                    "FROM BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE\n" +
                    "WHERE B.budget_month||' '||B.budget_year=:currentMonth\n" +
                    "GROUP BY B.DEPARTMENT_CODE,D.DEPARTMENT_NAME\n" +
                    "UNION ALL\n" +
                    "SELECT B1.DEPARTMENT_CODE,D.DEPARTMENT_NAME,0 TOTAL_BUDGET_PERBULAN,SUM(B1.TOTAL) TOTAL_BUDGET_SDBULAN,\n" +
                    "0 realisasi_per_bln,0 realisasi_sd_bln\n" +
                    "FROM BUDGET B1 JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B1.DEPARTMENT_CODE\n" +
                    "WHERE B1.budget_month||' '||B1.budget_year BETWEEN :fromMonth AND :toMonth \n" +
                    "GROUP BY B1.DEPARTMENT_CODE,D.DEPARTMENT_NAME\n" +
                    ")XX  GROUP BY XX.DEPARTMENT_CODE,XX.DEPARTMENT_NAME)\n" +
                    "WHERE PERSEN > 20";*/
//        String qry = "SELECT * FROM (\n"
//                + "SELECT DEPARTMENT_CODE,DEPARTMENT_NAME,\n"
//                + "SUM(TOTAL_BUDGET_PERBULAN)BUDGET_YTD_BULAN,\n"
//                + "SUM(realisasi_per_bln)REALISASI_YTD_BULAN,\n"
//                + "sum(TOTAL_BUDGET_PERBULAN-realisasi_per_bln) as YTD_BULAN,\n"
//                + "round(((case when sum(TOTAL_BUDGET_PERBULAN-realisasi_per_bln) < 0 then (sum(TOTAL_BUDGET_PERBULAN-realisasi_per_bln))*1 \n"
//                + "else sum(TOTAL_BUDGET_PERBULAN-realisasi_per_bln) end)\n"
//                + "/ (case when SUM(TOTAL_BUDGET_PERBULAN) > 0 then SUM(TOTAL_BUDGET_PERBULAN) else 1 end))*100,2) YTD_BULAN_PERSEN,\n"
//                + "SUM(TOTAL_BUDGET_SDBULAN) BUDGET_YTD_DES,\n"
//                + "SUM(realisasi_per_bln-TOTAL_BUDGET_SDBULAN)*-1 AS YTD_DES,\n"
//                + "round(((case when sum((realisasi_per_bln-TOTAL_BUDGET_SDBULAN)*-1) < 0 then (sum((realisasi_per_bln-TOTAL_BUDGET_SDBULAN)*-1))*1 \n"
//                + "else sum((realisasi_per_bln-TOTAL_BUDGET_SDBULAN)*-1) end)\n"
//                + "/ (case when SUM(TOTAL_BUDGET_SDBULAN) > 0 then SUM(TOTAL_BUDGET_SDBULAN) else 1 end))*100,2) YTD_DES_PERSEN\n"
//                + "FROM (\n"
//                + "SELECT IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME,0 TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,\n"
//                + "sum(ID.TOTAL) as realisasi_per_bln,0 realisasi_sd_bln\n"
//                + "FROM WORK_ORDER ID JOIN VEHICLE IH ON IH.ID=ID.ID_VEHICLE\n"
//                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n"
//                + "WHERE to_char(ID.wo_date,'MM yyyy')=:currentMonth GROUP BY IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME\n"
//                + "UNION ALL\n"
//                + "SELECT IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME,0 TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,\n"
//                + "0 realisasi_per_bln,sum(IDD.TOTAL) as realisasi_sd_bln\n"
//                + "FROM WORK_ORDER IDD JOIN VEHICLE IH ON IH.ID=IDD.ID_VEHICLE\n"
//                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n"
//                + "WHERE to_char(IDD.wo_date,'MM yyyy') BETWEEN :fromMonth AND :toMonth  GROUP BY IH.DEPARTMENT_CODE,D.DEPARTMENT_NAME\n"
//                + "UNION ALL\n"
//                + "SELECT B.DEPARTMENT_CODE,D.DEPARTMENT_NAME,SUM(B.TOTAL) TOTAL_BUDGET_PERBULAN,0 TOTAL_BUDGET_SDBULAN,\n"
//                + "0 realisasi_per_bln,0 realisasi_sd_bln\n"
//                + "FROM BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE\n"
//                + "WHERE B.budget_month||' '||B.budget_year=:currentMonth\n"
//                + "GROUP BY B.DEPARTMENT_CODE,D.DEPARTMENT_NAME\n"
//                + "UNION ALL\n"
//                + "SELECT B1.DEPARTMENT_CODE,D.DEPARTMENT_NAME,0 TOTAL_BUDGET_PERBULAN,SUM(B1.TOTAL) TOTAL_BUDGET_SDBULAN,\n"
//                + "0 realisasi_per_bln,0 realisasi_sd_bln\n"
//                + "FROM BUDGET B1 JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B1.DEPARTMENT_CODE\n"
//                + "WHERE B1.budget_month||' '||B1.budget_year BETWEEN :fromMonth AND :toMonth \n"
//                + "GROUP BY B1.DEPARTMENT_CODE,D.DEPARTMENT_NAME\n"
//                + ")XX  GROUP BY XX.DEPARTMENT_CODE,XX.DEPARTMENT_NAME)\n"
//                + "WHERE YTD_DES_PERSEN > 20";
        String qry = "SELECT DEPARTMENT_CODE,DEPARTMENT_NAME, sum(TOTAL_BUDGET_TAHUN) TOTAL_BUDGET_TAHUN,\n"
                + "SUM(REALISASI_CURRENTMONTH)REALISASI_CURRENTMONTH,\n"
                + "SUM(REALISASI_SD_CURRENTMONTH)REALISASI_SD_CURRENTMONTH,\n"
                + "SUM(TOTAL_BUDGET_TAHUN)-SUM(REALISASI_SD_CURRENTMONTH) AS SELISIH,\n"
                + "round(((case when sum(TOTAL_BUDGET_TAHUN-REALISASI_SD_CURRENTMONTH) < 0 then (sum(TOTAL_BUDGET_TAHUN-REALISASI_SD_CURRENTMONTH))*1 \n"
                + "else sum(TOTAL_BUDGET_TAHUN-REALISASI_SD_CURRENTMONTH) end)\n"
                + "/ (case when SUM(TOTAL_BUDGET_TAHUN) > 0 then SUM(TOTAL_BUDGET_TAHUN) else 1 end))*100,2) YTD_BULAN_PERSEN\n"
                + "FROM (SELECT WO.DEPARTMENT_CODE,D.DEPARTMENT_NAME,0 TOTAL_BUDGET_TAHUN,\n"
                + "SUM(TOTAL_REALISASI) AS REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH\n"
                + "FROM REALISASI R\n"
                + "JOIN WORK_ORDER WO ON WO.PRINT_NUMBER=R.PRINT_NUMBER\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=WO.DEPARTMENT_CODE WHERE to_char(R.TGL_REALISASI,'MM yyyy')=:currentMonth\n"
                + "GROUP BY WO.DEPARTMENT_CODE,D.DEPARTMENT_NAME\n"
                + "UNION ALL\n"
                + "-- jumlah realisasi servis kendaraan s/d bulan tertentu\n"
                + "SELECT WO.DEPARTMENT_CODE,D.DEPARTMENT_NAME,0 TOTAL_BUDGET_TAHUN,\n"
                + "0 REALISASI_CURRENTMONTH,SUM(TOTAL_REALISASI) REALISASI_SD_CURRENTMONTH\n"
                + "FROM REALISASI R\n"
                + "JOIN WORK_ORDER WO ON WO.PRINT_NUMBER=R.PRINT_NUMBER\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=WO.DEPARTMENT_CODE WHERE to_char(R.TGL_REALISASI,'MM yyyy') BETWEEN :fromMonth AND :currentMonth\n"
                + "GROUP BY WO.DEPARTMENT_CODE,D.DEPARTMENT_NAME\n"
                + "UNION ALL\n"
                + "SELECT B.DEPARTMENT_CODE,D.DEPARTMENT_NAME,SUM(B.TOTAL) AS TOTAL_BUDGET_TAHUN,0 REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH\n"
                + "FROM BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE \n"
                + "WHERE B.BUDGET_YEAR=:year GROUP BY B.DEPARTMENT_CODE,D.DEPARTMENT_NAME having count(B.STATUS_ADDITIONAL)>1\n"
                //                + "SELECT WO.DEPARTMENT_CODE,D.DEPARTMENT_NAME,SUM(B.TOTAL) AS TOTAL_BUDGET_TAHUN,0 REALISASI_CURRENTMONTH,0 REALISASI_SD_CURRENTMONTH\n"
                //                + "FROM WORK_ORDER WO JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=WO.DEPARTMENT_CODE\n"
                //                + "JOIN REALISASI R ON R.PRINT_NUMBER=WO.PRINT_NUMBER JOIN BUDGET B ON B.DEPARTMENT_CODE=WO.DEPARTMENT_CODE\n"
                //                + "WHERE B.BUDGET_YEAR=:year GROUP BY WO.DEPARTMENT_CODE,D.DEPARTMENT_NAME\n"
                + ")GROUP BY DEPARTMENT_CODE,DEPARTMENT_NAME";
        Map prm = new HashMap();
        prm.put("currentMonth", currentMonth);
        String a = currentMonth.substring(3, 7);
        String fromMonth = "01 " + a;
        String toMonth = "12 " + a;
        prm.put("fromMonth", fromMonth);
        prm.put("toMonth", toMonth);
        prm.put("year", a);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("departmentCode", rs.getString(1));
                rt.put("departmentName", rs.getString(2));
//                rt.put("budgetYtdBulan", rs.getString(3) == null ? "0" : rs.getString(3));
//                rt.put("realisasiYtdBulan", rs.getString(4) == null ? "0" : rs.getString(4));
//                rt.put("ytdBulan", rs.getString(5) == null ? "0" : rs.getString(5));
//                rt.put("ytdBulanPersen", rs.getString(6) == null ? "0" : rs.getString(6));
//                rt.put("budgetYtdDes", rs.getString(7) == null ? "0" : rs.getString(7));
//                rt.put("ytdDes", rs.getString(8) == null ? "0" : rs.getString(8));
//                rt.put("ytdDesPersen", rs.getString(9) == null ? "0" : rs.getString(9));
                rt.put("totalOverbudgetTahun", rs.getString(3) == null ? "0" : rs.getString(3));
                rt.put("realisasiCurrentmonth", rs.getString(4) == null ? "0" : rs.getString(4));
                rt.put("realisasiSdCurrentmonth", rs.getString(5) == null ? "0" : rs.getString(5));
                rt.put("selisih", rs.getString(6) == null ? "0" : rs.getString(6));
                rt.put("ytdBulanPersen", rs.getString(7) == null ? "0" : rs.getString(7));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> reportByRscYear(String currentMonth) {
//        String qry="SELECT RSC,SUM(JUM_PER_BLN) diservis,SUM(JUM_SD_BLN) terdaftar\n" +
//                    "FROM (\n" +
//                    "SELECT RSC,COUNT(JUM_PER_BLN)JUM_PER_BLN,SUM(JUM_SD_BLN)JUM_SD_BLN\n" +
//                    "FROM (\n" +
//                    "SELECT D.RSC,COUNT(IH.PLAT_NO) AS JUM_PER_BLN, 0 JUM_SD_BLN\n" +
//                    "FROM WORK_ORDER ID JOIN VEHICLE IH ON IH.ID=ID.ID_VEHICLE\n" +
//                    "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE AND ID.PLAT_NO=IH.PLAT_NO\n" +
//                    "WHERE to_char(ID.wo_date,'MM yyyy')BETWEEN :fromMonth AND :toMonth GROUP BY D.RSC,IH.PLAT_NO\n" +
//                    ")GROUP BY RSC\n" +
//                    "UNION ALL\n" +
//                    "SELECT D.RSC,0 JUM_PER_BLN,0 AS JUM_SD_BLN\n" +
//                    "FROM WORK_ORDER IDD JOIN VEHICLE IH ON IH.ID=IDD.ID_VEHICLE\n" +
//                    "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n" +
//                    "WHERE to_char(IDD.wo_date,'MM yyyy') BETWEEN :fromMonth AND :toMonth GROUP BY D.RSC\n" +
//                    "UNION ALL\n" +
//                    "SELECT D.RSC,0 JUM_PER_BLN,NVL(COUNT(IH.PLAT_NO),0) AS JUM_SD_BLN\n" +
//                    "FROM DEPARTMENT D LEFT JOIN VEHICLE IH ON IH.DEPARTMENT_CODE=D.DEPARTMENT_CODE\n" +
//                    "GROUP BY D.RSC\n" +
//                    "union all\n" +
//                    "SELECT D.RSC,0 JUM_PER_BLN,0 JUM_SD_BLN\n" +
//                    "FROM BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE\n" +
//                    "WHERE B.budget_month||' '||B.budget_year BETWEEN :fromMonth AND :toMonth \n" +
//                    "GROUP BY D.RSC\n" +
//                    "UNION ALL\n" +
//                    "SELECT D.RSC,0 JUM_PER_BLN,0 JUM_SD_BLN\n" +
//                    "FROM BUDGET B1 JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B1.DEPARTMENT_CODE\n" +
//                    "WHERE B1.budget_month||' '||B1.budget_year BETWEEN :fromMonth AND :toMonth \n" +
//                    "GROUP BY D.RSC\n" +
//                    ") GROUP BY RSC";
        String qry = "SELECT RSC,SUM(JUM_PER_BLN) diservis,SUM(JUM_SD_BLN) terdaftar\n"
                + "FROM (\n"
                + "SELECT RSC,COUNT(JUM_PER_BLN)JUM_PER_BLN,SUM(JUM_SD_BLN)JUM_SD_BLN\n"
                + "FROM (\n"
                + "SELECT D.RSC,COUNT(IH.PLAT_NO) AS JUM_PER_BLN, 0 JUM_SD_BLN\n"
                + "FROM WORK_ORDER ID JOIN VEHICLE IH ON IH.ID=ID.ID_VEHICLE\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE AND ID.PLAT_NO=IH.PLAT_NO\n"
                + "WHERE to_char(ID.wo_date,'MM yyyy')BETWEEN :fromMonth AND :toMonth GROUP BY D.RSC,IH.PLAT_NO\n"
                + ")GROUP BY RSC\n"
                + "UNION ALL\n"
                + "SELECT D.RSC,0 JUM_PER_BLN,0 AS JUM_SD_BLN\n"
                + "FROM WORK_ORDER IDD JOIN VEHICLE IH ON IH.ID=IDD.ID_VEHICLE\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE\n"
                + "WHERE to_char(IDD.wo_date,'MM yyyy') BETWEEN :fromMonth AND :toMonth GROUP BY D.RSC\n"
                + "UNION ALL\n"
                + "SELECT D.RSC,0 JUM_PER_BLN,NVL(COUNT(IH.PLAT_NO),0) AS JUM_SD_BLN\n"
                + "FROM DEPARTMENT D LEFT JOIN VEHICLE IH ON IH.DEPARTMENT_CODE=D.DEPARTMENT_CODE\n"
                + "GROUP BY D.RSC\n"
                + ") GROUP BY RSC";

        Map prm = new HashMap();
        prm.put("currentMonth", currentMonth);
        String a = currentMonth.substring(0, 4);
        String fromMonth = "01 " + a;
        String toMonth = "12 " + a;
        prm.put("fromMonth", fromMonth);
        prm.put("toMonth", toMonth);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("rsc", rs.getString(1));
                rt.put("diservis", rs.getString(2) == null ? "0" : rs.getString(2));
                rt.put("terdaftar", rs.getString(3) == null ? "0" : rs.getString(3));

                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listOpenWo(String param) {
        String edging = "";
        if (param.equalsIgnoreCase("1")) {
            edging = "BETWEEN '0' AND '30'";
        } else if (param.equalsIgnoreCase("2")) {
            edging = "BETWEEN '30' AND '60'";
        } else if (param.equalsIgnoreCase("3")) {
            edging = "BETWEEN '60' AND '90'";
        } else {
            edging = ">=90";
        }
        String qry = "SELECT WO.*,d.DEPARTMENT_NAME,V.USED_BY,W.WORK_SHOP_NAME FROM WORK_ORDER WO \n"
                + "join VEHICLE V on V.ID=WO.ID_VEHICLE join WORK_SHOP w on w.WORK_SHOP_CODE=WO.WORK_SHOP_CODE\n"
                + "join DEPARTMENT d on d.DEPARTMENT_CODE=WO.DEPARTMENT_CODE\n"
                + "WHERE WO.STATUS_REALISASI='N' AND ROUND(SYSDATE - TO_DATE(WO.WO_DATE),0) " + edging;
        Map prm = new HashMap();
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("kilometer", rs.getString("KILOMETER"));
                rt.put("platNo", rs.getString("PLAT_NO"));
                rt.put("workOrder", rs.getString("WORK_ORDER"));
                rt.put("workShopCode", rs.getString("WORK_SHOP_CODE"));
                rt.put("woDate", rs.getString("WO_DATE"));
                rt.put("spesification", rs.getString("SPESIFICATION"));
                rt.put("total", rs.getString("TOTAL"));
                rt.put("idVehicle", rs.getString("ID_VEHICLE"));
                rt.put("printNumber", rs.getString("PRINT_NUMBER"));
                rt.put("userCreate", rs.getString("USER_CREATE"));
                rt.put("dateCreate", rs.getString("DATE_CREATE"));
                rt.put("userUpdate", rs.getString("USER_UPDATE"));
                rt.put("dateUpdate", rs.getString("DATE_UPDATE"));
                rt.put("statusRealisasi", rs.getString("STATUS_REALISASI"));
                rt.put("departmentCode", rs.getString("DEPARTMENT_CODE"));
                rt.put("departmentName", rs.getString("DEPARTMENT_NAME"));
                rt.put("usedBy", rs.getString("USED_BY"));
                rt.put("workShopName", rs.getString("WORK_SHOP_NAME"));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listOverBudget(String year) {
        String qry = "SELECT B.*,D.DEPARTMENT_NAME FROM BUDGET B JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=B.DEPARTMENT_CODE \n"
                + "WHERE B.STATUS_ADDITIONAL='Y' and BUDGET_YEAR=:year";
        Map prm = new HashMap();
        prm.put("year", year);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("departmentCode", rs.getString("DEPARTMENT_CODE"));
                rt.put("total", rs.getString("TOTAL"));
                rt.put("budgetYear", rs.getString("BUDGET_YEAR"));
                rt.put("inputDate", rs.getString("INPUT_DATE"));
                rt.put("id", rs.getString("ID"));
                rt.put("statusAdditional", rs.getString("STATUS_ADDITIONAL"));
                rt.put("reasonAdditional", rs.getString("REASON_ADDITIONAL"));
                rt.put("departmentName", rs.getString("DEPARTMENT_NAME"));
                return rt;
            }
        });
        return list;
    }

    @Override
    public String getSaldoTotal(Budget budget) {
//            String SQL="SELECT NVL(TOTAL,0)AS TOTAL FROM BUDGET WHERE DEPARTMENT_CODE=:dept AND BUDGET_YEAR=:year AND STATUS_ADDITIONAL='N'";            
        String sql = "SELECT nvl(saldo_akhir,0) saldo_akhir FROM MUTASI_BUDGET WHERE DEPARTMENT_CODE=:dept and to_char(INPUT_DATE,'yyyy')=:year and ID in\n"
                + "(select max(id) from  MUTASI_BUDGET WHERE DEPARTMENT_CODE=:dept and to_char(INPUT_DATE,'yyyy')=:year)";

        Map param = new HashMap();
        param.put("dept", budget.getDepartmentCode());
        param.put("year", budget.getBudgetYear());
        System.out.println("Q JML TOTAL : " + sql);
        return jdbcTemplate.queryForObject(sql, param, new RowMapper() {
            @Override
            public Object mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString(1) == null ? "0" : rs.getString(1);
            }
        }).toString();
    }

    @Override
    public List<Map<String, Object>> listSaldoBudget(String dept, String year) {
        String qry = "SELECT NVL(TOTAL,0)AS TOTAL FROM BUDGET WHERE DEPARTMENT_CODE=:dept AND BUDGET_YEAR=:year AND STATUS_ADDITIONAL='N'";
        Map prm = new HashMap();
        prm.put("dept", dept);
        prm.put("year", year);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("total", rs.getString("TOTAL"));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> listVehicleByDept(String param) {
        String qry = "SELECT IH.*,D.DEPARTMENT_NAME FROM VEHICLE IH JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE WHERE IH.DEPARTMENT_CODE=:departmentCode";
        Map prm = new HashMap();
        prm.put("departmentCode", param);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("platNo", rs.getString(1));
                rt.put("pemakai", rs.getString(2));
                rt.put("departmentCode", rs.getString(3));
                rt.put("vehicle", rs.getString(4));
                rt.put("description", rs.getString(5));
                rt.put("vehicleType", rs.getString(6));
                rt.put("idVehicle", rs.getString(7));
                rt.put("tahun", rs.getString(8));
                rt.put("warna", rs.getString(9));
                rt.put("cc", rs.getString(10));
                rt.put("note", rs.getString(11));
                rt.put("departmentName", rs.getString(16));
                return rt;
            }
        });
        return list;
    }

    @Override
    public List<Map<String, Object>> tesReport(String platNo) {
        String qry = "SELECT IH.*,D.DEPARTMENT_NAME FROM VEHICLE IH JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=IH.DEPARTMENT_CODE "
                + "where IH.PLAT_NO=:platNo";

        Map prm = new HashMap();
        prm.put("platNo", platNo);
        System.err.println("q :" + qry);
        return jdbcTemplate.queryForList(qry, prm);
    }

    @Override
    public String getCountWo(String wo) {
//            String SQL="SELECT NVL(TOTAL,0)AS TOTAL FROM BUDGET WHERE DEPARTMENT_CODE=:dept AND BUDGET_YEAR=:year AND STATUS_ADDITIONAL='N'";            
        String sql = "SELECT NVL(COUNT(*),0)AS TOTAL FROM WORK_ORDER WHERE WORK_ORDER=:wo";

        Map param = new HashMap();
        param.put("wo", wo);
        System.out.println("Q JML TOTAL : " + sql);
        return jdbcTemplate.queryForObject(sql, param, new RowMapper() {
            @Override
            public Object mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString(1) == null ? "0" : rs.getString(1);
            }
        }).toString();
    }

    @Override
    public List<Map<String, Object>> listDetailServiceByDept(String dept) {
        String qry = "SELECT WO.*,V.USED_BY,D.DEPARTMENT_NAME,WS.WORK_SHOP_NAME,WS.WORK_SHOP_ADDRESS FROM WORK_ORDER WO LEFT JOIN VEHICLE V ON V.PLAT_NO=WO.PLAT_NO\n"
                + "JOIN DEPARTMENT D ON D.DEPARTMENT_CODE=WO.DEPARTMENT_CODE "
                + "JOIN WORK_SHOP WS ON WS.WORK_SHOP_CODE=WO.WORK_SHOP_CODE WHERE WO.DEPARTMENT_CODE=:dept ORDER BY WO.PRINT_NUMBER ASC";
        Map prm = new HashMap();
        prm.put("dept", dept);
        System.err.println("q :" + qry);
        List<Map<String, Object>> list = jdbcTemplate.query(qry, prm, new RowMapper<Map<String, Object>>() {
            @Override
            public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
                Map<String, Object> rt = new HashMap<String, Object>();
                rt.put("kilometer", rs.getString("KILOMETER"));
                rt.put("platNo", rs.getString("PLAT_NO"));
                rt.put("workOrder", rs.getString("WORK_ORDER"));
                rt.put("workShopCode", rs.getString("WORK_SHOP_CODE"));
                rt.put("woDate", rs.getString("WO_DATE"));
                rt.put("spesification", rs.getString("SPESIFICATION"));
                rt.put("total", rs.getString("TOTAL"));
                rt.put("idVehicle", rs.getString("ID_VEHICLE"));
                rt.put("printNumber", rs.getString("PRINT_NUMBER"));
                rt.put("workShopName", rs.getString("WORK_SHOP_NAME"));
                rt.put("workShopAddress", rs.getString("WORK_SHOP_ADDRESS"));
                rt.put("statusRealisasi", rs.getString("STATUS_REALISASI"));
                rt.put("departmentCode", rs.getString("DEPARTMENT_CODE"));
                rt.put("departmentName", rs.getString("DEPARTMENT_NAME"));
                rt.put("usedBy", rs.getString("USED_BY"));
                return rt;
            }
        });
        return list;
    }
}
