/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.dao.impl;

import com.ffi.apifpc.dao.MasterDAO;
import com.ffi.apifpc.model.Budget;
import com.ffi.apifpc.model.Department;
import com.ffi.apifpc.model.InputDetail;
import com.ffi.apifpc.model.InputHeader;
import com.ffi.apifpc.model.Realisasi;
import com.ffi.apifpc.model.WorkShop;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import com.ffi.apifpc.utils.DateUtils;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.dao.EmptyResultDataAccessException;

/**
 * 19 Jul 18
 *
 * @author Aryn
 */
@Repository
public class MasterDaoImpl implements MasterDAO {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public MasterDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(InputHeader inputHeader) {
        String qy = "INSERT INTO VEHICLE(PLAT_NO,USED_BY,DEPARTMENT_CODE,VEHICLE,DESCRIPTION,VEHICLE_TYPE,TAHUN,WARNA,CC,NOTE,USER_CREATE,DATE_CREATE,ID) "
                + "VALUES(:platNo, :usedBy, :departmentCode, :vehicle, :description,:vehicleType,:tahun,:warna,:cc,:note,:userCreate,:dateCreate,:id)";
        String qy2 = "INSERT INTO WORK_ORDER "
                + "VALUES (:kilometer,:platNo,:workOrder,:workShop,:woDate,:spesification,:total,:entryDate)";
        Map param = new HashMap();
        String id = getMaxIdVehicle();
        param.put("id", id);
        param.put("platNo", inputHeader.getPlatNo());
        param.put("usedBy", inputHeader.getUsedBy());
        param.put("departmentCode", inputHeader.getDepartmentCode());
        param.put("vehicle", inputHeader.getVehicle());
        param.put("description", inputHeader.getDescription());
        param.put("vehicleType", inputHeader.getVehicleType());
        param.put("tahun", inputHeader.getTahun());
        param.put("warna", inputHeader.getWarna());
        param.put("cc", inputHeader.getCc());
        param.put("note", inputHeader.getNote());
        param.put("userCreate", inputHeader.getUserCreate());
        param.put("dateCreate", DateUtils.newDate());
        jdbcTemplate.update(qy, param);
    }

    @Override
    public void delete(String id) {
        String qydel = "DELETE FROM VEHICLE WHERE ID=:id";
        String qydel2 = "DELETE FROM WORK_ORDER WHERE ID_VEHICLE=:idHeader";
        Map param = new HashMap();
        param.put("id", id);
        param.put("idHeader", id);
        jdbcTemplate.update(qydel, param);
        jdbcTemplate.update(qydel2, param);
    }

    @Override
    public void deleteDetail(InputDetail inputDetail) {
        String qymutasibudget = "INSERT INTO MUTASI_BUDGET VALUES (:departmentCode,:saldoAwal,:inputDate,:serviceAmount,:printNumberNew,:realisasiAmount,:saldoAkhir,:idmutasi,:ttf,:desc)";
        String qydel = "DELETE FROM WORK_ORDER WHERE PRINT_NUMBER=:printNumber";
        String qydel2 = "DELETE FROM MUTASI_BUDGET WHERE PRINT_NUMBER=:printNumber";
        Date yearWo = inputDetail.getWoDate();
        DateFormat sd = new SimpleDateFormat("yyyy");
        String printNumber = getId();
        String year = sd.format(yearWo);
        String saldo = getSaldoAkhir(inputDetail.getDepartmentCode(), year);
        double saldoakhir = Integer.parseInt(saldo);
        double total = inputDetail.getTotal();
        double totalSaldoAkhir = saldoakhir + total;
        Integer idmutasi = getIdMutasiBudget(inputDetail.getDepartmentCode(), year);

        Map param = new HashMap();
        param.put("printNumber", inputDetail.getPrintNumber());
        param.put("departmentCode", inputDetail.getDepartmentCode());
        param.put("idmutasi", idmutasi);
        param.put("printNumberNew", printNumber);
        param.put("saldoAwal", saldoakhir);
        param.put("serviceAmount", "0");
        param.put("inputDate", inputDetail.getWoDate());
        param.put("realisasiAmount", "0");
        param.put("saldoAkhir", totalSaldoAkhir);
        param.put("ttf", "-");
        param.put("desc", "PENGHAPUSAN SERVICE DENGAN WO : " + inputDetail.getPrintNumber() + " SEBESAR " + inputDetail.getTotal());
        jdbcTemplate.update(qydel, param);
        jdbcTemplate.update(qydel2, param);
        jdbcTemplate.update(qymutasibudget, param);
    }

    private String jumlahPlatNo(String platNo, String usedBy) {
        String sql = "SELECT count(nvl(PLAT_NO,0)) as jumlah FROM VEHICLE WHERE PLAT_NO=:platNo and USED_BY=:usedBy";
        System.out.println("SQL JML PLAT ==>" + sql);
        Map param = new HashMap();
        param.put("platNo", platNo);
        param.put("usedBy", usedBy);
        String nomor = jdbcTemplate.queryForObject(sql, param, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString(1) == null ? "0" : rs.getString(1);
            }
        });
        Integer value = Integer.valueOf(nomor);
        nomor = String.valueOf(value);
        return nomor;
    }

    @Override
    public void update(InputHeader inputHeader) {
        String qy = "UPDATE VEHICLE SET PLAT_NO=:platNo,USED_BY=:usedBy,DEPARTMENT_CODE=:departmentCode,VEHICLE=:vehicle,CC=:cc,"
                + "DESCRIPTION=:description,VEHICLE_TYPE=:vehicleType,TAHUN=:tahun,WARNA=:warna,NOTE=:note,USER_UPDATE=:userUpdate,DATE_UPDATE=:dateUpdate WHERE ID=:id";
        String qyInsHis = "INSERT INTO HISTORY VALUES (:platNoOld, :usedByOld, :departmentCodeOld, :vehicleOld, :descriptionOld,"
                + ":vehicleTypeOld,:tahunOld,:warnaOld,:ccOld,:noteOld,:userUpdate,:dateUpdate)";
        Map param = new HashMap();
        param.put("id", inputHeader.getId());
        param.put("platNo", inputHeader.getPlatNo());
        param.put("usedBy", inputHeader.getUsedBy());
        param.put("departmentCode", inputHeader.getDepartmentCode());
        param.put("vehicle", inputHeader.getVehicle());
        param.put("description", inputHeader.getDescription());
        param.put("vehicleType", inputHeader.getVehicleType());
        param.put("tahun", inputHeader.getTahun());
        param.put("warna", inputHeader.getWarna());
        param.put("cc", inputHeader.getCc());
        param.put("note", inputHeader.getNote());
        param.put("userUpdate", inputHeader.getUserUpdate());
        param.put("dateUpdate", DateUtils.newDate());

        param.put("platNoOld", inputHeader.getPlatNoOld());
        param.put("usedByOld", inputHeader.getUsedByOld());
        param.put("departmentCodeOld", inputHeader.getDepartmentCodeOld());
        param.put("vehicleOld", inputHeader.getVehicleOld());
        param.put("descriptionOld", inputHeader.getDescriptionOld());
        param.put("vehicleTypeOld", inputHeader.getVehicleTypeOld());
        param.put("tahunOld", inputHeader.getTahunOld());
        param.put("warnaOld", inputHeader.getWarnaOld());
        param.put("ccOld", inputHeader.getCcOld());
        param.put("noteOld", inputHeader.getNoteOld());
        param.put("userUpdate", inputHeader.getUserUpdate());
        param.put("dateUpdate", DateUtils.newDate());
        jdbcTemplate.update(qy, param);
        jdbcTemplate.update(qyInsHis, param);
    }

    @Override
    public void saveInput(InputHeader inputHeader, List<InputDetail> detail) {
        String qy = "INSERT INTO VEHICLE (PLAT_NO,USED_BY,DEPARTMENT_CODE,VEHICLE,DESCRIPTION,VEHICLE_TYPE) VALUES(:platNo, :usedBy, :departmentCode, :vehicle, :description,:vehicleType)";
        String qy2 = "INSERT INTO WORK_ORDER "
                + "VALUES (:kilometer,:platNo,:workOrder,:workShop,:woDate,:spesification,:total,:entryDate)";
        Map param = new HashMap();
        param.put("platNo", inputHeader.getPlatNo());
        param.put("usedBy", inputHeader.getUsedBy());
        param.put("departmentCode", inputHeader.getDepartmentCode());
        param.put("vehicle", inputHeader.getVehicle());
        param.put("description", inputHeader.getDescription());
        param.put("vehicleType", inputHeader.getVehicleType());
        jdbcTemplate.update(qy, param);
        for (InputDetail p : detail) {
            param.put("kilometer", inputHeader.getPlatNo());
//                param.put("headerNo", nomor);
            param.put("kilometer", p.getKilometer());
            param.put("workOrder", p.getWorkOrder());
            param.put("workShopCode", p.getWorkShopCode());
            param.put("woDate", p.getWoDate());
            param.put("spesification", p.getSpesification());
            param.put("total", p.getTotal());
            param.put("entryDate", DateUtils.newDate());
            jdbcTemplate.update(qy, param);
        }
    }

    @Override
    public void saveDetail(InputDetail inputDetail) {
        String qy = "INSERT INTO WORK_ORDER (KILOMETER,PLAT_NO,WORK_ORDER,WORK_SHOP_CODE,WO_DATE,SPESIFICATION,TOTAL,"
                + "ID_VEHICLE,USER_CREATE,DATE_CREATE,STATUS_REALISASI,DEPARTMENT_CODE,PRINT_NUMBER)"
                + "VALUES (:kilometer,:platNo,:workOrder,:workShopCode,:woDate,:spesification,:total,:idVehicle,:userCreate,:dateCreate,:statusRealisasi,:departmentCode,:printNumber)";
        String qymutasibudget = "INSERT INTO MUTASI_BUDGET VALUES (:departmentCode,:saldoAwal,:inputDate,:serviceAmount,:printNumber,:realisasiAmount,:saldoAkhir,:idmutasi,:ttf,:desc)";
        Date yearWo = inputDetail.getWoDate();
        DateFormat sd = new SimpleDateFormat("yyyy");
        String printNumber = getId();
        String year = sd.format(yearWo);
        String saldo = getSaldoAkhir(inputDetail.getDepartmentCode(), year);
        double saldoakhir = Integer.parseInt(saldo);
        double total = inputDetail.getTotal();
        double totalSaldoAkhir = saldoakhir - total;
        Integer idmutasi = getIdMutasiBudget(inputDetail.getDepartmentCode(), year);
        Map param = new HashMap();

        System.out.println("PN : " + printNumber);
        param.put("platNo", inputDetail.getPlatNo());
        param.put("kilometer", inputDetail.getKilometer());
        param.put("workOrder", inputDetail.getWorkOrder());
        param.put("workShopCode", inputDetail.getWorkShopCode());
        param.put("woDate", inputDetail.getWoDate());
        param.put("spesification", inputDetail.getSpesification());
        param.put("total", inputDetail.getTotal());
        param.put("idVehicle", inputDetail.getIdVehicle());
        param.put("userCreate", inputDetail.getUserCreate());
        param.put("dateCreate", DateUtils.newDate());
        param.put("statusRealisasi", "N");
        param.put("departmentCode", inputDetail.getDepartmentCode());
        param.put("idmutasi", idmutasi);
        param.put("printNumber", printNumber);
        param.put("saldoAwal", saldoakhir);
        param.put("serviceAmount", total);
        param.put("inputDate", inputDetail.getWoDate());
        param.put("realisasiAmount", "0");
        param.put("saldoAkhir", totalSaldoAkhir);
        param.put("ttf", "-");
        param.put("desc", "BIAYA SERVICE " + inputDetail.getTotal());
        jdbcTemplate.update(qy, param);
        jdbcTemplate.update(qymutasibudget, param);
    }

    @Override
    public void updateDetail(InputDetail inputDetail) {
        String qy = "UPDATE WORK_ORDER SET DEPARTMENT_CODE=:departmentCode,KILOMETER=:kilometer,WORK_ORDER=:workOrder,WORK_SHOP_CODE=:workShopCode,WO_DATE=:woDate,"
                + "SPESIFICATION=:spesification,TOTAL=:total,PLAT_NO=:platNo,ID_VEHICLE=:idVehicle,USER_UPDATE=:userUpdate,DATE_UPDATE=:dateUpdate WHERE PRINT_NUMBER=:printNumber";
        Map param = new HashMap();
        param.put("platNo", inputDetail.getPlatNo());
        param.put("kilometer", inputDetail.getKilometer());
        param.put("workOrder", inputDetail.getWorkOrder());
        param.put("workShopCode", inputDetail.getWorkShopCode());
        param.put("woDate", inputDetail.getWoDate());
        param.put("spesification", inputDetail.getSpesification());
        param.put("total", inputDetail.getTotal());
        param.put("printNumber", inputDetail.getPrintNumber());
        param.put("idVehicle", inputDetail.getIdVehicle());
        param.put("departmentCode", inputDetail.getDepartmentCode());
        param.put("userUpdate", inputDetail.getUserUpdate());
        param.put("dateUpdate", DateUtils.newDate());
        jdbcTemplate.update(qy, param);
    }

    @Override
    public void saveBudget(Budget budget) {
//        String qy = "INSERT INTO BUDGET(DEPARTMENT_CODE,BUDGET_MONTH,TOTAL,BUDGET_YEAR,INPUT_DATE,USER_CREATE) "
//                + "VALUES (:departmentCode,:budgetMonth,:total,:budgetYear,:inputDate,:userCreate)";
        String qy = "INSERT INTO BUDGET(ID,DEPARTMENT_CODE,TOTAL,BUDGET_YEAR,INPUT_DATE,USER_CREATE,STATUS_ADDITIONAL)"
                + "VALUES (:id,:departmentCode,:total,:budgetYear,:inputDate,:userCreate,:status)";
        String qymutasi = "INSERT INTO MUTASI_BUDGET VALUES (:departmentCode,:total,:inputDate,:serviceAmount,:printNumber,:realisasiAmount,:saldoAkhir,:idmutasi,:ttf,:desc)";
        Integer idmutasi = getIdMutasiBudget(budget.getDepartmentCode(), budget.getBudgetYear());
        Integer idbudget = getIdBudget(budget.getBudgetYear());
        Map param = new HashMap();
        NumberFormat formatter = new DecimalFormat("#0");
        System.out.println(formatter.format(budget.getTotal()));
        String total = formatter.format(budget.getTotal());
//        for (int i = 1; i <= 12; i++) {
        param = new HashMap();
        param.put("departmentCode", budget.getDepartmentCode());
        param.put("id", idbudget);
//            param.put("budgetMonth", String.format("%02d", i));
        param.put("total", total);
        param.put("budgetYear", budget.getBudgetYear());
        param.put("inputDate", DateUtils.newDate());
        param.put("userCreate", budget.getUserCreate());
        param.put("status", "N");
        param.put("serviceAmount", "0");
        param.put("printNumber", "-");
        param.put("realisasiAmount", "0");
        param.put("ttf", "-");
        param.put("saldoAkhir", total);
        param.put("idmutasi", idmutasi);
        param.put("desc", "BUDGET AWAL TAHUN " + budget.getBudgetYear());
        jdbcTemplate.update(qy, param);
        jdbcTemplate.update(qymutasi, param);
//        }
    }

    @Override
    public void updateBudget(Budget budget) {
        String qy = "UPDATE BUDGET SET TOTAL=:total,USER_UPDATE=:userUpdate,DATE_UPDATE=:dateUpdate WHERE ID=:id and BUDGET_YEAR=:budgetYear and DEPARTMENT_CODE=:departmentCode";
        String qymutasibudget = "INSERT INTO MUTASI_BUDGET VALUES (:departmentCode,:saldoAwal,:dateUpdate,:serviceAmount,:printNumber,"
                + ":realisasiAmount,:saldoAkhir,:idmutasi,:ttf,:desc)";
        Integer idmutasi = getIdMutasiBudget(budget.getDepartmentCode(), budget.getBudgetYear());
        String saldoakhir = getSaldoAkhir(budget.getDepartmentCode(), budget.getBudgetYear());
        // saldo akhir 220juta
        String saldobudget = getBudget(budget.getDepartmentCode(), budget.getBudgetYear());
        String jmlService = getCountServices(budget.getDepartmentCode(), budget.getBudgetYear());
        Integer convJmlService = Integer.valueOf(jmlService);
        String desc = "";

        double convSaldoAkhir = Integer.valueOf(saldoakhir);
        Integer convBudget = Integer.valueOf(saldobudget);
        //saldo budget 220juta
        double totalUpdateBudget = budget.getTotal();
        //diupdet menjadi 20juta
        double selisih = totalUpdateBudget - convBudget;
        //selisih = 20juta-220juta=200juta
        double totalSaldoAkhir = 0.0;
        BigDecimal bigDecimal = new BigDecimal(totalUpdateBudget);// form to BigDecimal
        BigDecimal bigDecimalSelisih = new BigDecimal(selisih);// form to BigDecimal
        String str = bigDecimal.toString();// get the String value
        String str2 = bigDecimalSelisih.toString();// get the String value
        if (convJmlService > 0) {
            desc = "PERUBAHAN SALDO BUDGET " + str + " SELISIH " + str2 + " DARI EDIT BUDGET";
            if (totalUpdateBudget > convBudget) {
                totalSaldoAkhir = convSaldoAkhir + selisih;

            } else {
                totalSaldoAkhir = convSaldoAkhir + selisih;
            }
        } else {
            totalSaldoAkhir = totalUpdateBudget;
            convSaldoAkhir = totalUpdateBudget;
            desc = "PERUBAHAN SALDO BUDGET AWAL TAHUN " + budget.getBudgetYear() + " MENJADI " + str;
        }

        Map param = new HashMap();
        param.put("departmentCode", budget.getDepartmentCode());
        param.put("total", budget.getTotal());
        param.put("budgetYear", budget.getBudgetYear());
        param.put("id", budget.getId());
        param.put("userUpdate", budget.getUserUpdate());
        param.put("dateUpdate", DateUtils.newDate());
        // Param mutasi budget
        param.put("saldoAwal", convSaldoAkhir);
        param.put("serviceAmount", "0");
        param.put("printNumber", "-");
        param.put("realisasiAmount", "0");
        param.put("saldoAkhir", totalSaldoAkhir);
        param.put("idmutasi", idmutasi);
        param.put("ttf", "-");
        param.put("desc", desc);
        jdbcTemplate.update(qy, param);
        jdbcTemplate.update(qymutasibudget, param);
    }

    @Override
    public void saveDepartment(Department dept) {
        String qy = "INSERT INTO DEPARTMENT VALUES (:departmentCode,:departmentName,:rsc,:coa)";
        Map param = new HashMap();
        param.put("departmentCode", dept.getDepartmentCode());
        param.put("departmentName", dept.getDepartmentName());
        param.put("rsc", dept.getRsc());
        param.put("coa", dept.getCoa());
        jdbcTemplate.update(qy, param);
    }

    @Override
    public void updateDepartment(Department dept) {
        String qy = "UPDATE DEPARTMENT SET DEPARTMENT_NAME=:departmentName,RSC=:rsc,COA=:coa WHERE DEPARTMENT_CODE=:departmentCode";
        Map param = new HashMap();
        param.put("departmentCode", dept.getDepartmentCode());
        param.put("departmentName", dept.getDepartmentName());
        param.put("rsc", dept.getRsc());
        param.put("coa", dept.getCoa());
        jdbcTemplate.update(qy, param);
    }

    @Override
    public void saveVendor(WorkShop vendor) {
        String qy = "INSERT INTO WORK_SHOP(WORK_SHOP_NAME,WORK_SHOP_ADDRESS,USER_CREATE,DATE_CREATE) "
                + "VALUES (:workShopName,:workShopAddress,:userCreate,:dateCreate)";
        Map param = new HashMap();
        param.put("workShopName", vendor.getWorkShopName());
        param.put("workShopAddress", vendor.getWorkShopAddress());
        param.put("userCreate", vendor.getUserCreate());
        param.put("dateCreate", DateUtils.newDate());
        jdbcTemplate.update(qy, param);
    }

    @Override
    public void updateVendor(WorkShop vendor) {
        String qy = "UPDATE WORK_SHOP SET WORK_SHOP_NAME=:workShopName,WORK_SHOP_ADDRESS=:workShopAddress,USER_UPDATE=:userUpdate,DATE_UPDATE=:dateUpdate "
                + "where WORK_SHOP_CODE=:workShopCode";
        Map param = new HashMap();
        param.put("workShopCode", vendor.getWorkShopCode());
        param.put("workShopName", vendor.getWorkShopName());
        param.put("workShopAddress", vendor.getWorkShopAddress());
        param.put("userUpdate", vendor.getUserUpdate());
        param.put("dateUpdate", DateUtils.newDate());
        jdbcTemplate.update(qy, param);
    }

    @Override
    public void deleteVendor(String workShopCode) {
        String qy = "DELETE FROM WORK_SHOP where WORK_SHOP_CODE=:workShopCode";
        Map param = new HashMap();
        param.put("workShopCode", workShopCode);
        jdbcTemplate.update(qy, param);
    }

    @Override
    public void addBudget(Budget budget) {
        String qy = "INSERT INTO BUDGET(ID,DEPARTMENT_CODE,TOTAL,BUDGET_YEAR,INPUT_DATE,USER_CREATE,STATUS_ADDITIONAL,REASON_ADDITIONAL)"
                + "VALUES (:id,:departmentCode,:total,:budgetYear,:inputDate,:userCreate,:status,:reason)";
        String qymutasibudget = "INSERT INTO MUTASI_BUDGET VALUES (:departmentCode,:saldoAwal,:inputDate,:serviceAmount,:printNumber,"
                + ":realisasiAmount,:saldoAkhir,:idmutasi,:ttf,:desc)";
        Integer idmutasi = getIdMutasiBudget(budget.getDepartmentCode(), budget.getBudgetYear());
        Integer idbudget = getIdBudget(budget.getBudgetYear());
        String saldoakhir = getSaldoAkhir(budget.getDepartmentCode(), budget.getBudgetYear());
        Integer convSaldoAkhir = Integer.valueOf(saldoakhir);
        double totalUpdateBudget = budget.getTotal();
        double totalSaldoAkhir = convSaldoAkhir + totalUpdateBudget;
        Map param = new HashMap();
        param = new HashMap();
        param.put("id", idbudget);
        param.put("departmentCode", budget.getDepartmentCode());
        param.put("total", budget.getTotal());
        param.put("budgetYear", budget.getBudgetYear());
        param.put("inputDate", DateUtils.newDate());
        param.put("userCreate", budget.getUserCreate());
        param.put("status", "Y");
        param.put("reason", budget.getReasonAdditional());
        // Param mutasi budget
        param.put("saldoAwal", convSaldoAkhir);
        param.put("serviceAmount", "0");
        param.put("printNumber", "-");
        param.put("realisasiAmount", "0");
        param.put("saldoAkhir", totalSaldoAkhir);
        param.put("idmutasi", idmutasi);
        param.put("ttf", "-");
        param.put("desc", "PENAMBAHAN SALDO BUDGET " + totalUpdateBudget + " DARI ADDITIONAL BUDGET");
        jdbcTemplate.update(qy, param);
        jdbcTemplate.update(qymutasibudget, param);
    }

    @Override
    public void realisasi(Realisasi realisasi) {
        String qy = "INSERT INTO REALISASI VALUES (:ttf,:printNumber,:total,:dateCreate,:userCreate,:tglRealisasi)";
        String qyUpd = "UPDATE WORK_ORDER SET STATUS_REALISASI='Y' WHERE PRINT_NUMBER=:printNumber";
        String qymutasi = "INSERT INTO MUTASI_BUDGET VALUES (:departmentCode,:saldoAwal,:inputDate,:serviceAmount,:printNumberMutasi,:realisasiAmount,:saldoAkhir,:idmutasi,:ttf,:desc)";
        String deptCode = getDeptCodeWo(realisasi.getPrintNumber());
        Date yearWo = realisasi.getTanggal();
        DateFormat sd = new SimpleDateFormat("yyyy");
        String year = sd.format(yearWo);
        Integer idmutasi = getIdMutasiBudget(deptCode, year);
        String saldoakhir = getSaldoAkhir(deptCode, year);
        Integer convSaldoAkhir = Integer.valueOf(saldoakhir);
        Integer total = Integer.valueOf(realisasi.getTotal());
        String serviceAmount = getServiceAmount(realisasi.getPrintNumber());
        Integer convServiceAmount = Integer.valueOf(serviceAmount);
        Integer totalSaldoAkhir = 0;
        Integer selisih;
        selisih = convServiceAmount - total;
        if (selisih == 0) {
            totalSaldoAkhir = convSaldoAkhir;
        } else if (selisih > 0) {
            totalSaldoAkhir = convSaldoAkhir + selisih;
        } else {
            totalSaldoAkhir = convSaldoAkhir - (Math.abs(selisih));
        }
        Map param = new HashMap();
        param.put("dateCreate", DateUtils.newDate());
        param.put("ttf", realisasi.getTtf());
        param.put("total", realisasi.getTotal());
        param.put("printNumber", realisasi.getPrintNumber());
        param.put("userCreate", realisasi.getUserCreate());
        param.put("tglRealisasi", realisasi.getTanggal());
        param.put("departmentCode", deptCode);
        param.put("saldoAwal", convSaldoAkhir);
        param.put("inputDate", realisasi.getTanggal());
        param.put("serviceAmount", "0");
        param.put("printNumberMutasi", "-");
        param.put("realisasiAmount", realisasi.getTotal());
        param.put("idmutasi", idmutasi);
        param.put("saldoAkhir", totalSaldoAkhir);
        param.put("desc", "BIAYA REALISASI " + realisasi.getTotal());
        jdbcTemplate.update(qy, param);
        jdbcTemplate.update(qyUpd, param);
        jdbcTemplate.update(qymutasi, param);
    }

    public String getId() {
        String sql = "SELECT MAX(SUBSTR(PRINT_NUMBER,11))AS PRINT_NUMBER FROM WORK_ORDER";
        Map param = new HashMap();
        System.err.println("get id : " + sql);
        String a = "WO";
        DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
        Date date = new Date();
        System.out.println("date : " + dateFormat.format(date));
        Integer nextId = 0;
        try {
            nextId = jdbcTemplate.queryForObject(sql, param, new RowMapper<Integer>() {
                @Override
                public Integer mapRow(ResultSet rs, int i) throws SQLException {
                    return rs.getInt(1) + 1;//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            });
        } catch (EmptyResultDataAccessException e) {
            nextId = 1;
        }
        return a + "-" + dateFormat.format(date).concat(String.format("%06d", nextId));
    }

    private String getMaxIdVehicle() {
        String sql = "SELECT MAX(NVL(ID,0)) AS ID FROM VEHICLE";
        System.out.println("SQL JML PLAT ==>" + sql);
        Map param = new HashMap();
        String nomor = jdbcTemplate.queryForObject(sql, param, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString(1) == null ? "0" : rs.getString(1);
            }
        });
        Integer value = Integer.valueOf(nomor) + 1;
        nomor = String.valueOf(value);
        return nomor;
    }

    @Override
    public void editAdditionalBudget(Budget budget) {
        String qy = "UPDATE BUDGET SET TOTAL=:total,USER_UPDATE=:userUpdate,DATE_UPDATE=:dateUpdate,STATUS_ADDITIONAL=:status,REASON_ADDITIONAL=:reason "
                + "WHERE ID=:id";
        String qymutasibudget = "INSERT INTO MUTASI_BUDGET VALUES (:departmentCode,:saldoAwal,:dateUpdate,:serviceAmount,:printNumber,"
                + ":realisasiAmount,:saldoAkhir,:idmutasi,:ttf,:desc)";
        Integer idmutasi = getIdMutasiBudget(budget.getDepartmentCode(), budget.getBudgetYear());
        String saldoakhir = getSaldoAkhir(budget.getDepartmentCode(), budget.getBudgetYear());
        String saldoAdditionalbudget = getAdditionalBudget(budget.getDepartmentCode(), budget.getBudgetYear());
        Integer convSaldoAkhir = Integer.valueOf(saldoakhir);
        Integer convBudget = Integer.valueOf(saldoAdditionalbudget);
        double totalUpdateBudget = budget.getTotal();
        double selisih = totalUpdateBudget - convBudget;
        double totalSaldoAkhir = convSaldoAkhir + selisih;
        Map param = new HashMap();
        param = new HashMap();
        param.put("departmentCode", budget.getDepartmentCode());
        param.put("total", budget.getTotal());
        param.put("budgetYear", budget.getBudgetYear());
        param.put("dateUpdate", DateUtils.newDate());
        param.put("userUpdate", budget.getUserUpdate());
        param.put("status", "Y");
        param.put("reason", budget.getReasonAdditional());
        param.put("id", budget.getId());
        // Param mutasi budget
        param.put("saldoAwal", convSaldoAkhir);
        param.put("serviceAmount", "0");
        param.put("printNumber", "-");
        param.put("realisasiAmount", "0");
        param.put("saldoAkhir", totalSaldoAkhir);
        param.put("idmutasi", idmutasi);
        param.put("ttf", "-");
        param.put("desc", "PENAMBAHAN SALDO BUDGET " + selisih + " DARI EDIT ADDITIONAL BUDGET");
        jdbcTemplate.update(qy, param);
        jdbcTemplate.update(qymutasibudget, param);
    }

    private String getSaldoAkhir(String dept, String year) {
//        String sql = "SELECT nvl(saldo_akhir,0) saldo_akhir FROM MUTASI_BUDGET WHERE DEPARTMENT_CODE=:dept and to_char(INPUT_DATE,'yyyy')=:year";
        String sql = "SELECT nvl(saldo_akhir,0) saldo_akhir FROM MUTASI_BUDGET WHERE DEPARTMENT_CODE=:dept and to_char(INPUT_DATE,'yyyy')=:year and ID in\n"
                + "(select max(id) from  MUTASI_BUDGET WHERE DEPARTMENT_CODE=:dept and to_char(INPUT_DATE,'yyyy')=:year)";
        System.out.println("SQL JML PLAT ==>" + sql);
        Map param = new HashMap();
        param.put("dept", dept);
        param.put("year", year);
        String nomor = jdbcTemplate.queryForObject(sql, param, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString(1) == null ? "0" : rs.getString(1);
            }
        });
        Integer value = Integer.valueOf(nomor);
        nomor = String.valueOf(value);
        return nomor;
    }

    private Integer getIdMutasiBudget(String dept, String year) {
        String sql = "select max(id*1) from MUTASI_BUDGET WHERE DEPARTMENT_CODE=:dept and to_char(INPUT_DATE,'yyyy')=:year";
        System.out.println("SQL JML PLAT ==>" + sql);
        Map param = new HashMap();
        param.put("dept", dept);
        param.put("year", year);
        Integer nextId = 0;
        try {
            nextId = jdbcTemplate.queryForObject(sql, param, new RowMapper<Integer>() {
                @Override
                public Integer mapRow(ResultSet rs, int i) throws SQLException {
                    return rs.getInt(1) + 1;//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            });
        } catch (EmptyResultDataAccessException e) {
            nextId = 1;
        }
        return nextId;
    }

    private String getDeptCodeWo(String printNumber) {
        String sql = "select department_code from work_order WHERE PRINT_NUMBER=:printNumber";
        System.out.println("SQL DEPT CODE ==>" + sql);
        Map param = new HashMap();
        param.put("printNumber", printNumber);
        String nomor = jdbcTemplate.queryForObject(sql, param, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString(1) == null ? "0" : rs.getString(1);
            }
        });

        return nomor;
    }

    private String getServiceAmount(String printNumber) {
        String sql = "select NVL(TOTAL,0)TOTAL from WORK_ORDER WHERE PRINT_NUMBER=:printNumber";
        System.out.println("SQL SERVICE AMOUNT ==>" + sql);
        Map param = new HashMap();
        param.put("printNumber", printNumber);
        String nomor = jdbcTemplate.queryForObject(sql, param, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString(1) == null ? "0" : rs.getString(1);
            }
        });
        return nomor;
    }

    private String getBudget(String dept, String year) {
        String sql = "SELECT nvl(TOTAL,0) BUDGET FROM BUDGET WHERE DEPARTMENT_CODE=:dept and to_char(INPUT_DATE,'yyyy')=:year and STATUS_ADDITIONAL='N'";
        System.out.println("SQL BUDGET ==>" + sql);
        Map param = new HashMap();
        param.put("dept", dept);
        param.put("year", year);
        String nomor = jdbcTemplate.queryForObject(sql, param, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString(1) == null ? "0" : rs.getString(1);
            }
        });
        Integer value = Integer.valueOf(nomor);
        nomor = String.valueOf(value);
        return nomor;
    }

    private Integer getIdBudget(String year) {
        String sql = "select max(id) from BUDGET WHERE BUDGET_YEAR=:year";
        System.out.println("SQL ID BUDGET ==>" + sql);
        Map param = new HashMap();
        param.put("year", year);
        Integer nextId = 0;
        try {
            nextId = jdbcTemplate.queryForObject(sql, param, new RowMapper<Integer>() {
                @Override
                public Integer mapRow(ResultSet rs, int i) throws SQLException {
                    return rs.getInt(1) + 1;//throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            });
        } catch (EmptyResultDataAccessException e) {
            nextId = 1;
        }
        return nextId;
    }

    private String getAdditionalBudget(String dept, String year) {
        String sql = "SELECT nvl(TOTAL,0) BUDGET FROM BUDGET WHERE DEPARTMENT_CODE=:dept and to_char(INPUT_DATE,'yyyy')=:year and STATUS_ADDITIONAL='Y'";
        System.out.println("SQL BUDGET ==>" + sql);
        Map param = new HashMap();
        param.put("dept", dept);
        param.put("year", year);
        String nomor = jdbcTemplate.queryForObject(sql, param, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString(1) == null ? "0" : rs.getString(1);
            }
        });
        Integer value = Integer.valueOf(nomor);
        nomor = String.valueOf(value);
        return nomor;
    }

    private String getCountServices(String dept, String year) {
//        String sql = "SELECT nvl(saldo_akhir,0) saldo_akhir FROM MUTASI_BUDGET WHERE DEPARTMENT_CODE=:dept and to_char(INPUT_DATE,'yyyy')=:year";
        String sql = "SELECT COUNT(*) AS JUMLAH FROM WORK_ORDER WHERE DEPARTMENT_CODE=:dept AND TO_CHAR(WO_DATE,'YYYY')=:year";
        System.out.println("SQL JML services ==>" + sql);
        Map param = new HashMap();
        param.put("dept", dept);
        param.put("year", year);
        String nomor = jdbcTemplate.queryForObject(sql, param, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int i) throws SQLException {
                return rs.getString(1) == null ? "0" : rs.getString(1);
            }
        });
        Integer value = Integer.valueOf(nomor);
        nomor = String.valueOf(value);
        return nomor;
    }
}
