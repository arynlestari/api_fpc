/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.model;

/**
 * 2 Jan 19
 * @author Aryn
 */
public class InputHeader {
    String platNo;
    String usedBy;
    String departmentCode;
    String vehicle;
    String description;
    String vehicleType;
    String id;
    String tahun;
    String warna;
    String cc;
    String note;
    String userCreate;
    String userUpdate;
    
    String platNoOld;
    String usedByOld;
    String departmentCodeOld;
    String vehicleOld;
    String descriptionOld;
    String vehicleTypeOld;
    String tahunOld;
    String warnaOld;
    String ccOld;
    String noteOld;
    String userCreateOld;
    String userUpdateOld;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNoteOld() {
        return noteOld;
    }

    public void setNoteOld(String noteOld) {
        this.noteOld = noteOld;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getUserCreateOld() {
        return userCreateOld;
    }

    public void setUserCreateOld(String userCreateOld) {
        this.userCreateOld = userCreateOld;
    }

    public String getUserUpdateOld() {
        return userUpdateOld;
    }

    public void setUserUpdateOld(String userUpdateOld) {
        this.userUpdateOld = userUpdateOld;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getWarna() {
        return warna;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getTahunOld() {
        return tahunOld;
    }

    public void setTahunOld(String tahunOld) {
        this.tahunOld = tahunOld;
    }

    public String getWarnaOld() {
        return warnaOld;
    }

    public void setWarnaOld(String warnaOld) {
        this.warnaOld = warnaOld;
    }

    public String getCcOld() {
        return ccOld;
    }

    public void setCcOld(String ccOld) {
        this.ccOld = ccOld;
    }

    public String getPlatNoOld() {
        return platNoOld;
    }

    public void setPlatNoOld(String platNoOld) {
        this.platNoOld = platNoOld;
    }

    public String getUsedByOld() {
        return usedByOld;
    }

    public void setUsedByOld(String usedByOld) {
        this.usedByOld = usedByOld;
    }

    public String getDepartmentCodeOld() {
        return departmentCodeOld;
    }

    public void setDepartmentCodeOld(String departmentCodeOld) {
        this.departmentCodeOld = departmentCodeOld;
    }

    public String getVehicleOld() {
        return vehicleOld;
    }

    public void setVehicleOld(String vehicleOld) {
        this.vehicleOld = vehicleOld;
    }

    public String getDescriptionOld() {
        return descriptionOld;
    }

    public void setDescriptionOld(String descriptionOld) {
        this.descriptionOld = descriptionOld;
    }

    public String getVehicleTypeOld() {
        return vehicleTypeOld;
    }

    public void setVehicleTypeOld(String vehicleTypeOld) {
        this.vehicleTypeOld = vehicleTypeOld;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlatNo() {
        return platNo;
    }

    public void setPlatNo(String platNo) {
        this.platNo = platNo;
    }

    public String getUsedBy() {
        return usedBy;
    }

    public void setUsedBy(String usedBy) {
        this.usedBy = usedBy;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
}
