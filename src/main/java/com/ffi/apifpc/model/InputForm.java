/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 4 Jan 19
 * @author Aryn
 */
public class InputForm {
    InputHeader inputHeader;
    List<InputDetail> detail = new ArrayList<InputDetail>();

    public InputHeader getInputHeader() {
        return inputHeader;
    }

    public void setInputHeader(InputHeader inputHeader) {
        this.inputHeader = inputHeader;
    }

    public List<InputDetail> getDetail() {
        return detail;
    }

    public void setDetail(List<InputDetail> detail) {
        this.detail = detail;
    }
}
