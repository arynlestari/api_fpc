/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.model;

/**
 * 21 Jan 19
 * @author Aryn
 */
public class WorkShop {
    String workShopCode;
    String workShopName;
    String workShopAddress;
    String userCreate;
    String userUpdate;

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }
    
    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getWorkShopCode() {
        return workShopCode;
    }

    public void setWorkShopCode(String workShopCode) {
        this.workShopCode = workShopCode;
    }

    public String getWorkShopName() {
        return workShopName;
    }

    public void setWorkShopName(String workShopName) {
        this.workShopName = workShopName;
    }

    public String getWorkShopAddress() {
        return workShopAddress;
    }

    public void setWorkShopAddress(String workShopAddress) {
        this.workShopAddress = workShopAddress;
    }

   }
