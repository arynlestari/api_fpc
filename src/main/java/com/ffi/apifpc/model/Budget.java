/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.model;

import java.util.Date;

/**
 * 8 Jan 19
 * @author Aryn
 */
public class Budget {
    String departmentCode;
    String budgetMonth;
    String budgetYear;
    double total;
    Date inputDate;
    String id;
    String userCreate;
    String userUpdate;
    Date dateUpdate;
    String statusAdditional;
    String reasonAdditional;

    public String getStatusAdditional() {
        return statusAdditional;
    }

    public void setStatusAdditional(String statusAdditional) {
        this.statusAdditional = statusAdditional;
    }

    public String getReasonAdditional() {
        return reasonAdditional;
    }

    public void setReasonAdditional(String reasonAdditional) {
        this.reasonAdditional = reasonAdditional;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getBudgetMonth() {
        return budgetMonth;
    }

    public void setBudgetMonth(String budgetMonth) {
        this.budgetMonth = budgetMonth;
    }

    public String getBudgetYear() {
        return budgetYear;
    }

    public void setBudgetYear(String budgetYear) {
        this.budgetYear = budgetYear;
    }

    public Date getInputDate() {
        return inputDate;
    }

    public void setInputDate(Date inputDate) {
        this.inputDate = inputDate;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    
}
