/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.services;

import com.ffi.apifpc.utils.DateUtils;
import com.ffi.apifpc.utils.RptParameter;
import com.ffi.apifpc.utils.XlsRptBaseAll;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfTable;
import com.lowagie.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author user
 */
public class ReportServices extends XlsRptBaseAll {

    List<Map<String, Object>> listStore = new ArrayList<Map<String, Object>>();
    HttpServletResponse response;
    String year;

    public ReportServices(HttpServletResponse response, List<Map<String, Object>> listStore) {
        this.response = response;
        this.listStore = listStore;
    }

    @Override
    public List<RptParameter> getParameters() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getTemplateName() {
        return "sample";
    }

    @Override
    public String getTemplateNameSum() {
        return "data_summary";
    }

    @Override
    public String getTemplatePath() {
        String uri = "pdf/";
        return uri;
    }

    public String destination() {
        return "http://localhost:6979/pdfreport";
    }

    @Override
    public void generate() throws Exception {
        PdfPTable table = new PdfPTable(3);
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        table.setWidthPercentage(60);
        table.setWidths(new int[]{1, 3, 3});

        Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        PdfPCell hcell;

        hcell = new PdfPCell(new Phrase("Id", headFont));
        hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(hcell);

        hcell = new PdfPCell(new Phrase("Name", headFont));
        hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(hcell);

        hcell = new PdfPCell(new Phrase("Population", headFont));
        hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(hcell);
        for (Map d : listStore) {
            PdfPCell cell;
            cell = new PdfPCell(new Phrase(d.get("VEHICLE").toString()));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
        }
        PdfWriter.getInstance(document, out);
        document.open();
        document.add(table);
//        document.newPage();
        document.close();
//        pushToPdfFile(destination());
    }

    public void generateOLD() throws Exception {
        loadWorkbookXlsx();
        loadSheet(0);
        int startRow = 4;
        int no = 1;
        updateCell(0, 0, "NSO " + year);
        updateCell(19, 0, DateUtils.newDateComplete());
        for (Map d : listStore) {
            updateCell(0, startRow, no);
            updateCell(1, startRow, d.get("NAMA_OUTLET").toString());
            updateCell(2, startRow, (String) d.get("KOTA"));
            updateCell(3, startRow, (String) d.get("ALAMAT") + " Tlp. " + (d.get("TELEPON") == null ? "-" : d.get("TELEPON")));
            updateCell(4, startRow, (String) (d.get("LONGITUDE") == null ? "" : d.get("LONGITUDE")) + ", " + (d.get("LATITUDE") == null ? "" : d.get("LATITUDE")));
            updateCell(5, startRow, (String) d.get("ROM"));
            updateCell(6, startRow, (String) d.get("TIPE_STORE"));
            BigDecimal buMS = (BigDecimal) (d.get("BU_MS") == null ? BigDecimal.ZERO : d.get("BU_MS"));
            BigDecimal buHD = (BigDecimal) (d.get("BU_HD") == null ? BigDecimal.ZERO : d.get("BU_HD"));
            BigDecimal buDT = (BigDecimal) (d.get("BU_DT") == null ? BigDecimal.ZERO : d.get("BU_DT"));
            BigDecimal buCS = (BigDecimal) (d.get("BU_CS") == null ? BigDecimal.ZERO : d.get("BU_CS"));
            updateCell(7, startRow, buMS);
            updateCell(8, startRow, buHD);
            updateCell(9, startRow, buDT);
            updateCell(10, startRow, buCS);
            Double totalProjection = buMS.doubleValue() + buHD.doubleValue() + buDT.doubleValue() + buCS.doubleValue();
            updateCell(11, startRow, totalProjection);
            updateCell(12, startRow, (String) d.get("TIPE_CLASS"));
            updateCell(13, startRow, (String) d.get("STATUS_LOCATION"));
            updateCell(14, startRow, (String) d.get("STATUS_PROGRES"));
            updateCell(15, startRow, (String) d.get("KONDISI_STORE"));
            updateCell(16, startRow, DateUtils.toString((Date) d.get("TANGGAL_BUKA"), DateUtils.DATE_FORMAT_D_MMMM_Y));
            updateCell(17, startRow, 3);
            BigDecimal targetPoint = (BigDecimal) (d.get("TARGET_POINT") == null ? BigDecimal.ZERO : d.get("TARGET_POINT"));
            updateCell(18, startRow, targetPoint);
            Double totalMS = buMS.doubleValue() * targetPoint.doubleValue();
            Double totalSalesProjection = totalProjection * targetPoint.doubleValue();
            updateCell(19, startRow, totalMS);
            updateCell(20, startRow, totalSalesProjection);
            updateCell(21, startRow, (String) d.get("REMARKS"));
            copyRowStyle(startRow, startRow + 1, 22);
            startRow++;
            no++;
        }
        pushToClientXlsx(response, getTemplatePath(), getTemplateName());
    }

    @Override
    public void generateSummary(String outletCode, String wilayah) throws Exception {
        loadWorkbookXls();
        loadSheet(0);
        int startRow = 9;
        int no = 1;
        if (wilayah.equalsIgnoreCase("")) {
            updateCell(0, 2, "Wilayah : -");
        } else {
            updateCell(0, 2, "Wilayah : " + (listStore.size() > 0 ? listStore.get(0).get("Wilayah") : wilayah));
        }

        if (outletCode.equalsIgnoreCase("")) {
            updateCell(0, 3, "Outlet : -");
        } else {
            updateCell(0, 3, "Outlet : " + (listStore.size() > 0 ? listStore.get(0).get("OUTLET_NAME") : "-"));
        }
        updateCell(0, 5, DateUtils.toString(DateUtils.newDateComplete(), DateUtils.DATE_FORMAT_COMPLETE));
        for (Map d : listStore) {
            updateCell(0, startRow, no);
            updateCell(1, startRow, (String) d.get("OUTLET_NAME"));
            updateCell(2, startRow, (String) d.get("OPENING_DATE"));
            updateCell(3, startRow, (String) d.get("ADDRESS"));
            updateCell(4, startRow, (String) d.get("FACILITY_TYPE"));
            updateCell(5, startRow, (String) d.get("CITY_NAME"));
            updateCell(6, startRow, (String) d.get("PROVINSI"));
            updateCell(7, startRow, (String) d.get("UPLUKL"));
            updateCell(8, startRow, (String) d.get("IMB"));
            updateCell(9, startRow, (String) d.get("TDP"));
            updateCell(10, startRow, (String) d.get("SKDP_EXP"));
            updateCell(11, startRow, (String) d.get("SKDP_KET"));
            updateCell(12, startRow, (String) d.get("IZIN_GANGGUAN_EXP"));
            updateCell(13, startRow, (String) d.get("IZIN_GANGGUAN_KET"));
            updateCell(14, startRow, (String) d.get("SERTIFIKAT_LAIK"));
            updateCell(15, startRow, (String) d.get("RKM_EXP"));
            updateCell(16, startRow, (String) d.get("RKM_KET"));
            updateCell(17, startRow, (String) d.get("TDUP_EXP"));
            updateCell(18, startRow, (String) d.get("TDUP_KET"));
            updateCell(19, startRow, (String) d.get("INSTALASI_LISTRIK"));
            updateCell(20, startRow, (String) d.get("GENSET"));
            updateCell(21, startRow, (String) d.get("LIFT_BARANG"));
            updateCell(22, startRow, (String) d.get("INSTALASI_PETIR"));
            updateCell(23, startRow, (String) d.get("APAR"));
            updateCell(24, startRow, "");
            updateCell(25, startRow, "");
            copyRowStyle(startRow, startRow + 1, 26);
            startRow++;
            no++;
        }
        pushToClientXlsx(response, getTemplatePath(), getTemplateNameSum());
    }
}
