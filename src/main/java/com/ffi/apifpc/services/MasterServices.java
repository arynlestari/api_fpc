/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.services;

import com.ffi.apifpc.dao.MasterDAO;
import com.ffi.apifpc.model.Budget;
import com.ffi.apifpc.model.Department;
import com.ffi.apifpc.model.InputDetail;
import com.ffi.apifpc.model.InputHeader;
import com.ffi.apifpc.model.Realisasi;
import com.ffi.apifpc.model.WorkShop;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 2 Jan 19
 * @author Aryn
 */
@Service
public class MasterServices {
    @Autowired
    MasterDAO dao;
    
    
    public void save(InputHeader inputHeader){
        dao.save(inputHeader);
    }
    
    public void delete(String id){
        dao.delete(id);
    }
    
    public void deleteDetail(InputDetail inputdetail){
        dao.deleteDetail(inputdetail);
    }
    
    public void update(InputHeader inputHeader){
        dao.update(inputHeader);
    }
    
    public void saveInput(InputHeader header,List<InputDetail> detail){
        dao.saveInput(header, detail);
    }
    
    public void saveDetail(InputDetail inputDetail){
        dao.saveDetail(inputDetail);
    }
    
    public void updateDetail(InputDetail inputDetail){
        dao.updateDetail(inputDetail);
    }
    
    public void saveBudget(Budget budget){
        dao.saveBudget(budget);
    }
    
    public void updateBudget(Budget budget){
        dao.updateBudget(budget);
    }
    
    public void saveDept(Department dept){
        dao.saveDepartment(dept);
    }
    
    public void updateDept(Department dept){
        dao.updateDepartment(dept);
    }
    
    public void saveVendor(WorkShop vendor){
        dao.saveVendor(vendor);
    }
    
    public void updateVendor(WorkShop vendor){
        dao.updateVendor(vendor);
    }
    
    public void deleteVendor(String vendorCode){
        dao.deleteVendor(vendorCode);
    }
    
    public void addBudget(Budget budget){
        dao.addBudget(budget);
    }
    
    public void realisasi(Realisasi realisasi){
        dao.realisasi(realisasi);
    }
    
    public void editAdditionalBudget(Budget budget){
        dao.editAdditionalBudget(budget);
    }
}
