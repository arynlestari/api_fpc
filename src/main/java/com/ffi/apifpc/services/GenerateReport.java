/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.services;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aryn
 */
public class GenerateReport {

    public static ByteArrayInputStream budgetReport(String platno,List<Map<String, Object>> list) {

        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {

            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(60);
            table.setWidths(new int[]{2, 3, 4});

            Font fontbold = FontFactory.getFont("Times-Roman", 12, Font.BOLD);
            Font reg = FontFactory.getFont("Times-Roman", 12, Font.NORMAL);
            Paragraph title = new Paragraph("Mutasi Budget", fontbold);
            Paragraph subtitle = new Paragraph("Department : "+platno, reg);
            title.setSpacingAfter(5);
            title.setAlignment(1); // Center
            subtitle.setSpacingAfter(20);
            subtitle.setAlignment(1);
            Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

            PdfPCell hcell;
            hcell = new PdfPCell(new Phrase("Plat No", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Used By", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            hcell = new PdfPCell(new Phrase("Vehicle", headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);

            for (Map d : list) {

                PdfPCell cell;

                cell = new PdfPCell(new Phrase(d.get("PLAT_NO").toString()));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(d.get("USED_BY").toString()));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(d.get("VEHICLE").toString()));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
            }

            PdfWriter.getInstance(document, out);
            document.open();
            document.add(title);
            document.add(subtitle);
            document.add(table);

            document.close();

        } catch (DocumentException ex) {

            Logger.getLogger(GenerateReport.class.getName()).log(Level.ALL.SEVERE, null, ex);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }
}
