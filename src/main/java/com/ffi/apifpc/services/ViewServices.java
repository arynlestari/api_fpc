/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc.services;

import com.ffi.apifpc.dao.ViewDAO;
import com.ffi.apifpc.model.Budget;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 9 Jan 19
 * @author Aryn
 */
@Service
public class ViewServices {
    @Autowired
    ViewDAO dao;
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listAllVehicle(){
        return dao.listAllVehicle();
    }
    
    @Transactional(readOnly = true)    
    public List<Map<String, Object>> listVehicleByType(String vehicleType,String departmentCode){
        return dao.listVehicleByType(vehicleType, departmentCode);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listAllBudget(String tahun,String dept){
        return dao.listAllBudget(tahun, dept);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listDeptByYear(String tahun){
        return dao.listDeptByYear(tahun);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listAllDept(){
        return dao.listAllDept();
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listRekap(String param){
        return dao.listRekap(param);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listRekapDetail(String tahun,String platNo){
        return dao.listRekapDetail(tahun, platNo);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listDetail(String param){
        return dao.listDetail(param);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listRekapByDept(String tahun,String dept){
        return dao.listRekapByDept(tahun, dept);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listRekapDetailByDept(String tahun,String dept){
        return dao.listRekapDetailByDept(tahun, dept);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listAllVendor(){
        return dao.listAllVendor();
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> reportByDept(String currentMonth){
        return dao.reportByDept(currentMonth);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> reportByRsc(String currentMonth){
        return dao.reportByRsc(currentMonth);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> reportBudgetOver20(String currentMonth){
        return dao.reportBudgetOver20(currentMonth);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> reportByRscYear(String currentMonth){
        return dao.reportByRscYear(currentMonth);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listOpenWo(String param){
        return dao.listOpenWo(param);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listOverBudget(String param){
        return dao.listOverBudget(param);
    }
    
    public String getSaldoTotal(Budget budget){
        return dao.getSaldoTotal(budget);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listSaldoBudget(String dept,String year){
        return dao.listSaldoBudget(dept, year);
    }
    
    @Transactional(readOnly = true)    
    public List<Map<String, Object>> listVehicleByDept(String param){
        return dao.listVehicleByDept(param);
    }
    
    public List<Map<String, Object>> tesReport(String platNo){
        return dao.tesReport(platNo);
    }
    
    public String getCountWo(String wo){
        return dao.getCountWo(wo);
    }
    
    @Transactional(readOnly = true)
    public List<Map<String, Object>> listDetailServiceByDept(String dept){
        return dao.listDetailServiceByDept(dept);
    }
}
