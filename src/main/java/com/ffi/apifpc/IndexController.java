/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ffi.apifpc;

import com.ffi.apifpc.model.Budget;
import com.ffi.apifpc.model.Department;
import com.ffi.apifpc.model.InputDetail;
import com.ffi.apifpc.model.InputForm;
import com.ffi.apifpc.model.InputHeader;
import com.ffi.apifpc.model.Realisasi;
import com.ffi.apifpc.model.WorkShop;
import com.ffi.apifpc.services.GenerateReport;
import com.ffi.apifpc.services.MasterServices;
import com.ffi.apifpc.services.ReportServices;
import com.ffi.apifpc.services.StorageService;
import com.ffi.apifpc.services.ViewServices;
import com.ffi.paging.Response;
import com.ffi.paging.ResponseMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import static java.lang.System.out;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 2 Jan 19
 *
 * @author Aryn
 */
@RestController
public class IndexController {

    @Autowired
    MasterServices services;

    @Autowired
    ViewServices viewServices;

    @Autowired
    StorageService storageService;

    @RequestMapping(value = "/halo.action")
    public @ResponseBody
    Map<String, Object> tes() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("output", "yo men");
        return map;
    }

    @RequestMapping(value = "/list-vehicle-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan data kendaraan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listAllVehicle() {
        Gson gsn = new Gson();
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listAllVehicle());
        return res;
    }

    @RequestMapping(value = "/list-vehicleType-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan data kendaraan berdasarkan jenis kendaraan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listVehicleByType(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listVehicleByType(map.get("vehicleType").toString(), map.get("departmentCode").toString()));
        return res;
    }

    @RequestMapping(value = "/save.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menyimpan data kendaraan (data header)", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage save(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        InputHeader inputHeader = gsn.fromJson(param, new TypeToken<InputHeader>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.save(inputHeader);
            rm.setSuccess(true);
            rm.setMessage("Insert Master Data Input Header Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Insert Master Data Input Header Failed");
//            map.put("item", e.getMessage());
        }
//        list.add(map);
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/save-detail.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menyimpan data services kendaraan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage saveDetail(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        InputDetail inputDetail = gsn.fromJson(param, new TypeToken<InputDetail>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.saveDetail(inputDetail);
            rm.setSuccess(true);
            rm.setMessage("Insert Master Data Input Detail Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Insert Master Data Input Detail Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/update-detail.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk mengedit data detail kendaraan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage updateDetail(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        InputDetail inputDetail = gsn.fromJson(param, new TypeToken<InputDetail>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.updateDetail(inputDetail);
            rm.setSuccess(true);
            rm.setMessage("Update Master Data Input Detail Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Update Master Data Input Detail Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/delete-detail.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menghapus data detail servis kendaraan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage deleteDetail(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        InputDetail inputDetail = gsn.fromJson(param, new TypeToken<InputDetail>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.deleteDetail(inputDetail);
            rm.setSuccess(true);
            rm.setMessage("Delete Master Input Detail Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Delete Master Input Detail Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/update.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk mengubah data kendaraan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage update(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        InputHeader inputHeader = gsn.fromJson(param, new TypeToken<InputHeader>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.update(inputHeader);
            rm.setSuccess(true);
            rm.setMessage("Update Master Data Input Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Update Master Data Input Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/delete.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menghapus data kendaraan header", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage delete(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.delete(map.get("id").toString());
            rm.setSuccess(true);
            rm.setMessage("Delete Master Data Input Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Delete Master Data Input Failed");
            map.put("item", e.getMessage());
        }
        list.add(map);
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/save-budget.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menyimpan data budget", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage saveBudget(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        Budget budget = gsn.fromJson(param, new TypeToken<Budget>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.saveBudget(budget);
            rm.setSuccess(true);
            rm.setMessage("Insert Master Budget Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Insert Master Budget Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/update-budget.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk mengubah data budget", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage updateBudget(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        Budget budget = gsn.fromJson(param, new TypeToken<Budget>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.updateBudget(budget);
            rm.setSuccess(true);
            rm.setMessage("Update Master Budget Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Update Master Budget Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/list-budget-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan data budget", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listAllBudget(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listAllBudget(map.get("tahun").toString(), map.get("dept").toString()));
        return res;
    }

    @RequestMapping(value = "/list-dept-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan data department", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listAllDept(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listAllDept());
        return res;
    }

    @RequestMapping(value = "/list-dept-budget-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan data department yang belum ada di list budget", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listDeptByYear(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listDeptByYear(map.get("tahun").toString()));
        return res;
    }

    @RequestMapping(value = "/save-dept.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menyimpan data budget", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage saveDept(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        Department dept = gsn.fromJson(param, new TypeToken<Department>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.saveDept(dept);
            rm.setSuccess(true);
            rm.setMessage("Insert Master Department Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Insert Master Department Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/update-dept.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menyimpan data budget", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage updateDept(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        Department dept = gsn.fromJson(param, new TypeToken<Department>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.updateDept(dept);
            rm.setSuccess(true);
            rm.setMessage("Update Master Department Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Update Master Department Failed");
        }
        rm.setItem(list);
        return rm;
    }

    /*
    Untuk menyimpan header & detail kendaraan jika dijadikan 1 penginputan
     */
    @RequestMapping(value = "/save-input.action", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseMessage prosesSave(@RequestBody String param) {
        Gson gsn = new Gson();
        JsonReader reader = new JsonReader(new StringReader(param.trim()));
        reader.setLenient(true);
        InputForm formHeader = gsn.fromJson(reader, new TypeToken<InputForm>() {
        }.getType());
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.saveInput(formHeader.getInputHeader(), formHeader.getDetail());
            rm.setSuccess(true);
            rm.setMessage("Save Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Save Failed");
            map.put("item", e.getMessage());
        }
        list.add(map);
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/list-rekap-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan rekap data kendaraan per tahun", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listRekap(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listRekap(map.get("tahun").toString()));
        return res;
    }

    @RequestMapping(value = "/list-rekapdetail-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan rekap data kendaraan per tahun per plat no", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listRekapDetail(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listRekapDetail(map.get("tahun").toString(), map.get("platNo").toString()));
        return res;
    }

    @RequestMapping(value = "/list-detail-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan detail data kendaraan per plat no", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listDetail(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listDetail(map.get("platNo").toString()));
        return res;
    }

    @RequestMapping(value = "/list-rekap-bydept-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan rekap data kendaraan per tahun per dept", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listRekapByDept(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listRekapByDept(map.get("tahun").toString(), map.get("dept").toString()));
        return res;
    }

    @RequestMapping(value = "/list-rekapdetail-bydept-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan rekap detail data kendaraan per tahun per dept", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listRekapDetailByDept(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listRekapDetailByDept(map.get("tahun").toString(), map.get("dept").toString()));
        return res;
    }

    @RequestMapping(value = "/list-workshop-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan data vendor", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listAllVendor() {
        Gson gsn = new Gson();
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listAllVendor());
        return res;
    }

    @RequestMapping(value = "/save-workshop.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menyimpan data Vendor/Workshop", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage saveVendor(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        WorkShop vendor = gsn.fromJson(param, new TypeToken<WorkShop>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.saveVendor(vendor);
            rm.setSuccess(true);
            rm.setMessage("Insert Master Vendor/Workshop Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Insert Master Vendor Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/update-workshop.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menyimpan data Vendor/Workshop", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage updateVendor(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        WorkShop budget = gsn.fromJson(param, new TypeToken<WorkShop>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.updateVendor(budget);
            rm.setSuccess(true);
            rm.setMessage("Update Master Vendor/WorkShop Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Update Master Vendor/WorkShop Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/delete-workshop.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menghapus data vendor/workshop", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage deleteVendor(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.deleteVendor(map.get("workShopCode").toString());
            rm.setSuccess(true);
            rm.setMessage("Delete Master Vendor/Workshop Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Delete Master Vendor/Workshop Failed");
            map.put("item", e.getMessage());
        }
        list.add(map);
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/report-bydept-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan laporan vehicle realisasi dan budget", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response reportByDept(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.reportByDept(map.get("currentMonth").toString()));
        return res;
    }

    @RequestMapping(value = "/report-byrsc-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan laporan vehicle realisasi dan budget", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response reportByRsc(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.reportByRsc(map.get("currentMonth").toString()));
        return res;
    }

    @RequestMapping(value = "/report-overbudget-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan laporan vehicle realisasi dan budget diatas 20%", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response reportBudgetOver20(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.reportBudgetOver20(map.get("currentMonth").toString()));
        return res;
    }

    @RequestMapping(value = "/report-byrsc-year-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan laporan jumlah vehicle yang diservis dan terdaftar sepanjang tahun", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response reportByRscYear(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.reportByRscYear(map.get("year").toString()));
        return res;
    }

    // Upload vehicle
    // import data bankbook untuk rekonsil
    @RequestMapping(value = "/upload-vehicle.action") // //new annotation since 4.3
    public @ResponseBody
    Map<String, Object> uploadDataBankBook(@RequestParam MultipartFile file, String user) throws FileNotFoundException, IOException {
        Map<String, Object> map = new HashMap<String, Object>();
        Connection sambungan = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
            //Menyambungkan aplikasi dengan database
            sambungan = DriverManager.getConnection("jdbc:oracle:thin:@192.168.10.4:1521:orcl", "fpc", "kfc14022");
            //Menggunakan Prepared Statement
            PreparedStatement ps = sambungan.prepareStatement("INSERT INTO INPUT_HEADER(PLAT_NO,USED_BY,DEPARTMENT_CODE,VEHICLE"
                    + ",DESCRIPTION,VEHICLE_TYPE,ID,TAHUN,WARNA,CC,USER_CREATE,DATE_CREATE)"
                    + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
            if (!sambungan.isClosed()) {
                out.println("Connected");
            }
            storageService.store(file);
            InputStream ExcelFileToRead = new FileInputStream(new File("upload" + "/" + file.getOriginalFilename()));
            HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);
            HSSFSheet sheet = wb.getSheetAt(0);
            HSSFRow row;
            HSSFCell cell;
            String platNo = "";
            String usedBy = "";
            String deptCode = "";
            String vehicle = "";
            String desc = "";
            double id = 0;
            String vehicleType = "";
            double tahun = 0;
            String warna = "";
            double cc = 0;
//            String user="Administrator";
            //FORMAT TANGGAL
            String accdate = "";
            Date tgl = new Date(System.currentTimeMillis());
            Iterator rows = sheet.rowIterator();
            DataFormatter formatter = new DataFormatter();
            int i = 0;
            while (rows.hasNext()) {
                row = (HSSFRow) rows.next();
                i += 1;
                if (i > 1) {
                    Iterator cells = row.cellIterator();
                    while (cells.hasNext()) {
                        cell = (HSSFCell) cells.next();
//                        if (cell.getColumnIndex() == 11) {
//                            String tanggalx=formatter.formatCellValue(cell);
//                            java.util.Date date = new SimpleDateFormat("dd-MMM-yy").parse(tanggalx);
//                            accdate = DateUtils.toString(date, DateUtils.DATE_FORMAT_D_MMM_Y);
//                        }
                        if (cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                            if (cell.getColumnIndex() == 1) {
                                platNo = cell.getStringCellValue();
                            } else if (cell.getColumnIndex() == 2) {
                                usedBy = cell.getStringCellValue();
                            } else if (cell.getColumnIndex() == 3) {
                                deptCode = cell.getStringCellValue();
                            } else if (cell.getColumnIndex() == 4) {
                                vehicle = cell.getStringCellValue();
                            } else if (cell.getColumnIndex() == 5) {
                                desc = cell.getStringCellValue();
                            } else if (cell.getColumnIndex() == 6) {
                                vehicleType = cell.getStringCellValue();
                            } else if (cell.getColumnIndex() == 8) {
                                warna = cell.getStringCellValue();
                            }

                            System.out.print(cell.getStringCellValue() + " ");
                        } else if (cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                            if (cell.getColumnIndex() == 0) {
                                id = cell.getNumericCellValue();
                            } else if (cell.getColumnIndex() == 7) {
                                tahun = cell.getNumericCellValue();
                            } else if (cell.getColumnIndex() == 9) {
                                cc = cell.getNumericCellValue();
                            }
                            System.out.print(cell.getNumericCellValue() + " ");
                        }
                    }

                    ps.setString(1, platNo);
                    ps.setString(2, usedBy);
                    ps.setString(3, deptCode);
                    ps.setString(4, vehicle);
                    ps.setString(5, desc);
                    ps.setString(6, vehicleType);
                    ps.setDouble(7, id);
                    ps.setDouble(8, tahun);
                    ps.setString(9, warna);
                    ps.setDouble(10, cc);
                    ps.setString(11, user);
                    ps.setDate(12, tgl);
                    try {
                        ps.execute();
                        System.out.println("berhasil");
                    } catch (SQLException ex) {
                        System.out.println("/nDUPLICATE");
                    }
                }
                System.out.println();
            }
            try {
                //tutup sambungan
                sambungan.close();
                map.put("success", true);
                System.out.println("berhasil");
            } catch (SQLException ex) {
                map.put("success", false);
            }
        } catch (SQLException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

        }
        return map;
    }

    @RequestMapping(value = "/add-budget.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menyimpan data tambahan budget", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage addBudget(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        Budget budget = gsn.fromJson(param, new TypeToken<Budget>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.addBudget(budget);
            rm.setSuccess(true);
            rm.setMessage("Additional Budget Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Additional Budget Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/realisasi.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menyimpan data kendaraan (data header)", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage realisasi(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        Realisasi realisasi = gsn.fromJson(param, new TypeToken<Realisasi>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.realisasi(realisasi);
            rm.setSuccess(true);
            rm.setMessage("Realisasi Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Realisasi Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/list-openwo-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan data budget", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listOpenWo(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listOpenWo(map.get("edging").toString()));
        return res;
    }

    @RequestMapping(value = "/edit-additional-budget.action", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menyimpan data tambahan budget", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    ResponseMessage editAdditionalBudget(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        Budget budget = gsn.fromJson(param, new TypeToken<Budget>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            services.editAdditionalBudget(budget);
            rm.setSuccess(true);
            rm.setMessage("Edit Additional Budget Success");
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Edit Additional Budget Failed");
        }
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/list-overbudget-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan rekap data kendaraan per tahun per plat no", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listOverBudget(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listOverBudget(map.get("year").toString()));
        return res;
    }

    @RequestMapping(value = "/validasi-budget.action", produces = "application/json")
    public @ResponseBody
    ResponseMessage validasibudget(@RequestBody Budget budget) throws JRException, IOException, Exception {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            double totalBudgetParam = budget.getTotal();
            double totalbudget = Double.valueOf(totalBudgetParam);
            String last = viewServices.getSaldoTotal(budget);
            double totBudget = Double.valueOf(last);
            if (totBudget >= totalbudget) {
                rm.setSuccess(true);
                rm.setMessage("Budget mencukupi");
                map.put("item", totBudget);
            } else if (totalbudget >= totBudget) {
                rm.setSuccess(false);
                rm.setMessage("Melebihi Saldo Budget");
                map.put("item", totBudget);
            } else if (totBudget <= 0) {
                rm.setSuccess(false);
                rm.setMessage("Saldo budget 0");
            } else {
                rm.setSuccess(false);
                rm.setMessage("Budget tidak mencukupi");
                map.put("item", totBudget);
            }
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Cek Budget Failed");
            map.put("item", "Belum ada budget untuk department ini");
        }
        list.add(map);
        rm.setItem(list);
        return rm;
    }

    @RequestMapping(value = "/validasi-budget-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan rekap data kendaraan per tahun per plat no", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listSaldoBudget(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listSaldoBudget(map.get("departmentCode").toString(), map.get("budgetYear").toString()));
        return res;
    }

    @RequestMapping(value = "/list-vehicleDept-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan data kendaraan berdasarkan jenis kendaraan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listVehicleByDept(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listVehicleByDept(map.get("departmentCode").toString()));
        return res;
    }

    @RequestMapping(value = "/x", method = RequestMethod.GET, produces = "application/vnd.pdf")
    public void getRptDataStoreXls(HttpServletRequest request, HttpServletResponse response) {
        try {
//            ReportServices xls = new ReportServices(response, viewServices.tesReport());
//            xls.generate();
        } catch (Exception ex) {
            Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    @RequestMapping(value = "/pdfreport", method = RequestMethod.GET,produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> citiesReport(String platNo) throws IOException {

        List<Map<String, Object>> list=viewServices.tesReport(platNo);
        ByteArrayInputStream bis = GenerateReport.budgetReport(platNo,list);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=tes.pdf");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
    
    @RequestMapping(value = "/validasi-wo.action", produces = "application/json")
    public @ResponseBody
    ResponseMessage validasiwo(@RequestBody String param) throws JRException, IOException, Exception {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        ResponseMessage rm = new ResponseMessage();
        try {
            String last = viewServices.getCountWo(map.get("workorder").toString());
            double totalwo = Double.valueOf(last);
            if (totalwo >= 1) {
                rm.setSuccess(false);
                rm.setMessage("WO Sudah ada");
                map.put("item", totalwo);
            } else {
                rm.setSuccess(true);
                rm.setMessage("WO belum pernah diinput");
                map.put("item", totalwo);
            }
        } catch (Exception e) {
            rm.setSuccess(false);
            rm.setMessage("Cek WO Failed");
            map.put("item", "Belum ada wo untuk service ini");
        }
        list.add(map);
        rm.setItem(list);
        return rm;
    }
    
    @RequestMapping(value = "/list-detailwo-dept-json", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Digunakan untuk menampilkan detail data service per department", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK")
        ,
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public @ResponseBody
    Response listDetailWoDept(@RequestBody String param) {
        Gson gsn = new Gson();
        Map<String, Object> map = gsn.fromJson(param, new TypeToken<Map<String, Object>>() {
        }.getType());
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        Response res = new Response();
        res.setData(viewServices.listDetailServiceByDept(map.get("departmentCode").toString()));
        return res;
    }
}
